<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/css/dataTables.bootstrap.min.css" rel="stylesheet"/> -->
<div class="alertes container">
  <h1>Le tableau de bord des alertes</h1>

  <?php
  global $wpdb;
  $table   = $wpdb->prefix . "abonnes";
  $sql     = "select * from " . $table;
  $abonnes = $wpdb->get_results($sql, OBJECT); ?>

  <section class="alerte-table">
    <table id="example" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
      <thead>
        <tr>
          <th>email</th>
          <th>date</th>
          <th>langue</th>
        </tr>
      </thead>
      <tbody> <?php
        foreach($abonnes as $abonne){
          echo "<tr>
                  <td>$abonne->email</td>
                  <td>$abonne->dateAb</td>
                  <td>".getLangue($abonne->langue)."</td>
                </tr>";
        }
      ?>
      </tbody>
    </table>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js"></script>
    <script src="../script.js"></script>
  </section>
  <?php $sql       = "select count(*) as nbAbonnes from " . $table;
        $result    = $wpdb->get_results($sql, OBJECT);
        $nbAbonnes = $result[0]->nbAbonnes;

        $sql       = "select count(*) as nbAbonnes, langue from " . $table . " group by langue";
        $result    = $wpdb->get_results($sql, OBJECT);
?>
  <section>
    <h3>Abonnés : <?php echo $nbAbonnes; ?> </h3>
    <ul>
      <?php
        foreach ($result as $value) { ?>
          <li>
            <p>Soit <?php echo $value->nbAbonnes ?> : <?php echo getLangue($value->langue); ?></p>
          </li>
      <?php
        }
      ?>
    </ul>
  </section>
<?php
  $table     = $wpdb->prefix . "desabonnements";
  $sql       = "select nombre  from $table";
  $result    = $wpdb->get_results($sql, OBJECT);
  $desabonnements = $result[0]->nombre;
?>
  <section>
    <h3>Désabonnements : <?php echo $desabonnements; ?> </h3>
  </section>
</div>

<?php
  function getLangue($langue){
    switch ($langue) {
        case "fr-FR": $langue = "FR" ;
            break;
        case "es-ES": $langue = "ES" ;
            break;
        case "en-GB": $langue = "EN" ;
            break;
    }
    return $langue;
  }
?>
