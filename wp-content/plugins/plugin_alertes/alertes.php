<?php
/*
Plugin Name: plugin-alertes
Description: Envoie d'un mail (publication)
Version: 0.1
Author: Urbilog
*/

register_activation_hook(__FILE__,'abonnes_database_install');

function abonnes_database_install() {

	global $wpdb;

	$table = $wpdb->prefix . "abonnes";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table . " (email varchar(100) NOT NULL PRIMARY KEY, langue TEXT, token TEXT, dateAb TEXT) ENGINE=InnoDB";
	$wpdb->query($sql);

	$table = $wpdb->prefix . "desabonnements";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table . " (nombre INT NOT NULL) ENGINE=InnoDB";
	$wpdb->query($sql);

	$sql = "INSERT INTO $table(nombre) VALUES(0)";
	$wpdb->query($sql);

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
}

// Add the hook action
add_action('transition_post_status', 'send_new_app', 10, 3);

// Listen for publishing of a new APP
function send_new_app($new_status, $old_status, $post) {
	if('publish' === $new_status && 'publish' !== $old_status && $post->post_type === 'app') {
		global $wpdb;
    $table = $wpdb->prefix . "abonnes";
    $post_lang = pll_get_post_language($post->ID , 'slug');
    switch($post_lang){
      case "en" : $post_lang = "en-GB";
        break;
      case "es" : $post_lang = 'es-ES';
        break;
      case "fr" : $post_lang = 'fr-FR';
        break;
    }

		$LANG = get_term_lang_alerte($post_lang);
		$domaine = get_bloginfo("wpurl");

    $sql = "select * from ". $table . ' where langue = "' . $post_lang . '"';
    $abonnes = $wpdb->get_results($sql, OBJECT);

		$url = get_permalink($post->ID);
    $titre = wp_strip_all_tags(get_the_title($post->ID));
		$categoriesArray = get_the_terms($post->ID, 'app_category');
		$categories = '';
		if($categoriesArray != false){
			foreach($categoriesArray as $categorie){
				$categories .= $categorie->name . ' / ';
			}
		}


		$headers = 'From: ISITE-ULNE <noreply@isite-ulne.fr>';


		foreach($abonnes as $abonne){
			$email = $abonne->email;
			$token = $domaine .'/unsubscribe/index.php?lang='.$post_lang.'&token='.$abonne->token.'&email='.$abonne->email;

			$message = str_replace('{lien}',$url,$LANG->URBILOG_ALERTES_APP_CONTENU);
			$message = str_replace('{titre}',$titre,$message);
			$message = str_replace('{categories}', $categories, $message);
			$message = str_replace('{token}',$token,$message);

			wp_mail($email, $titre, $message, $headers);
		}
	}
}


// Add the hook action
add_action('transition_post_status', 'send_new_post', 10, 3);

// Listen for publishing of a new post
function send_new_post($new_status, $old_status, $post) {
  if('publish' === $new_status && 'publish' !== $old_status && $post->post_type === 'post') {

    global $wpdb;
    $table = $wpdb->prefix . "abonnes";
    $post_lang = pll_get_post_language($post->ID , 'slug');
    switch($post_lang){
      case "en" : $post_lang = "en-GB";
									$date = get_the_date('Y-m-d',$post->ID);
        break;
      case "es" : $post_lang = 'es-ES';
									$date = get_the_date('d/m/Y',$post->ID);
        break;
      case "fr" : $post_lang = 'fr-FR';
									$date = get_the_date('d/m/Y',$post->ID);
        break;
    }

		// Get file.json
		$LANG = get_term_lang_alerte($post_lang);
		$domaine = get_bloginfo("wpurl");

    $sql = "select * from ". $table . ' where langue = "' . $post_lang . '"';
    $abonnes = $wpdb->get_results($sql, OBJECT);

		$url = get_permalink($post->ID);
		$img = get_the_post_thumbnail_url($post->ID);
    $titre = wp_strip_all_tags(get_the_title($post->ID));
		$categoriesArray = get_the_category($post->ID);
		$categories = '';

		foreach($categoriesArray as $categorie){
			$categories .= $categorie->cat_name . ' / ';
		}

    $headers = 'From: ISITE-ULNE <noreply@isite-ulne.fr>';

    foreach($abonnes as $abonne){
      $email = $abonne->email;
			$token = $domaine .'/unsubscribe/index.php?lang='.$post_lang.'&token='.$abonne->token.'&email='.$abonne->email;

			$message = str_replace('{lien}',$url,$LANG->URBILOG_ALERTES_CONTENU);
			$message = str_replace('{image}',$img,$message);
			$message = str_replace('{date}',$date,$message);
			$message = str_replace('{titre}',$titre,$message);
			$message = str_replace('{categories}', $categories, $message);
			$message = str_replace('{token}',$token,$message);

      wp_mail($email, $titre, $message, $headers);
    }

  }
}

add_filter('wp_mail_content_type', function( $content_type ) {
            return 'text/html';
});

/**
 * Add shortcode
**/
add_shortcode('Urbilog-alertes', 'show_form_alertes');
/**
 * Show form and insert
 *
 */
function show_form_alertes() {

	$LANG = get_term_lang_alertes();
	 if(isset($_POST['email_newsletters']) && empty($_POST['email_newsletters'])){
		$msg = $LANG->URBILOG_ALERTES_OBLIGATOIRE;
	 }else{
		 if(filter_var($_POST['email_newsletters'], FILTER_VALIDATE_EMAIL)) { // valid address
			$msg = $LANG->URBILOG_ALERTES_SUCCES;
			$get = '?newsletter=msg';

    	global $wpdb;
    	$table_name = $wpdb->prefix . "abonnes";
      $email = $_POST['email_newsletters'];
      $lang  = get_bloginfo("language");
      $token = bin2hex(random_bytes(20));
			$date  = date('Y-m-d');
    	$wpdb->insert($table_name,array('email'=> $email, 'langue' => $lang ,'token' => $token, 'dateAb' => $date));

		}
		else {
			$msg = $LANG->URBILOG_ALERTES_ERROR_EMAIL;
			$get = '?newsletter=msg';
		}
	}
	if(!empty($msg)){
		$span = '<span style="text-align:center; border: 2px solid #f05915; padding: 1rem; display: block; width: 50%; margin: auto;margin-bottom: 1rem; margin-left:0;" class="form-msg" tabindex="0">'.$msg.'</span>';
	}
	return '<form action="'.get_permalink().$get.'" class="newsletter" id="newsletter" method="post">'.$span.'
	<div class="content" style=""><label for="email_newsletters" style="display: block; margin-bottom:1rem;"> Email : </label><input required style="display: block; margin-bottom:1rem;" name="email_newsletters" id="email" type="email" placeholder="adresse@gmail.com" />
	<input type="submit" style="display: block;" class="newsletter_submit" name="" value="'.$LANG->URBILOG_ALERTES_ENVOYER.'"/></div></form>';
}

// Add to admin menu
add_action('admin_menu', 'add_menu_plugin_alertes');

function add_menu_plugin_alertes(){
	add_menu_page( 'alertes', 'Tableau de bords des alertes', 'manage_options', 'plugin_alertes_index', 'add_plugin_alertes_index','dashicons-controls-volumeon' );
}

function add_plugin_alertes_index() {
    include('controller/index.php');
}

// Add style to admin plugin
// add_action( 'admin_enqueue_scripts', 'plugin_style_alertes' );

function plugin_style_alertes() {
	wp_enqueue_style( 'style_alertes', plugin_dir_url(__FILE__).'style.css' );
	wp_enqueue_script( 'js_alertes', plugin_dir_url( __FILE__ ) . 'script.js', array('jquery'), '1.0', true );
}

// load the scripts on only the plugin admin page
if (isset($_GET['page']) && ($_GET['page'] == 'plugin_alertes_index'))
{
  // if we are on the plugin page, enable the script
	add_action('admin_enqueue_scripts', 'plugin_style_alertes');
}

/**
 * Get current lang
 *
 */
function get_term_lang_alertes(){
    $lang = get_bloginfo("language");
    $domaine = get_bloginfo("wpurl");
    $file = "fr.json" ;
    $rep  = "lang" ;
    switch ($lang) {
        case "fr-FR": $file = "fr.json" ;
            break;
        case "es-ES": $file = "es.json" ;
            break;
        case "en-GB": $file = "en.json" ;
            break;
    }
	$content = wp_remote_get($domaine.'/wp-content/plugins/plugin_alertes/lang/'.$file);
    return json_decode($content["body"]);
}

function get_term_lang_alerte($lang){
	$domaine = get_bloginfo("wpurl");
	switch($lang){
		case "en-GB" : $file = "en.json" ;
			break;
		case "es-ES" : $file = "es.json" ;
			break;
		case "fr-FR" : $file = "fr.json";
			break;
	}
	$content = wp_remote_get($domaine.'/wp-content/plugins/plugin_alertes/lang/'.$file);
  return json_decode($content["body"]);}
