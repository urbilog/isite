<?php
/*
Plugin Name: plugin-carrousel
Description: Gestion du carrousel
Version: 0.1
Author: Urbilog
*/

ini_set("display_errors", 1);

register_activation_hook(__FILE__,'carrousel_database_install'); 

// register_deactivation_hook( __FILE__, 'database_delete' );

add_action('admin_menu', 'add_menu_plugin_carrousel');

function carrousel_database_install() {

	global $wpdb;

	$table1 = $wpdb->prefix . "motcles";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table1 . " (id_motcle int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, titre TEXT, titre_en TEXT,ordre int(10)) ENGINE=InnoDB";
	$wpdb->query($sql);

	$table2 = $wpdb->prefix . "slides";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table2 . " (id_slide int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, src TEXT, id_motcle int(10), color TEXT, caption TEXT, CONSTRAINT fk_motcle FOREIGN KEY (id_motcle) REFERENCES  ".$table1."(id_motcle) ON DELETE CASCADE) ENGINE=InnoDB";
	$wpdb->query($sql);

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
}

add_action( 'admin_enqueue_scripts', 'plugin_carrousel_style' );

function plugin_carrousel_style() {
	wp_enqueue_media();
	wp_enqueue_style( 'carrousel_style', plugin_dir_url(__FILE__).'carrousel.css' );
	wp_enqueue_style( 'carrousel_bootstrap', plugin_dir_url(__FILE__).'bootstrap.urbilog.css');
	wp_enqueue_script( 'carrousel_color_script', plugin_dir_url(__FILE__).'nativeColorPicker.js', array(), '1.0' );
	wp_enqueue_script( 'carrousel_script', plugin_dir_url( __FILE__ ) . 'carrousel.js', array(), '1.0' );
}

function add_menu_plugin_carrousel(){
	add_menu_page( 'Carrousel', 'Carrousel', '', 'plugin_carrousel_index', 'add_plugin_carrousel_index','dashicons-images-alt' );
	add_submenu_page( 'plugin_carrousel_index', 'Tous les mots clés', 'Tous les mots clés', 'read', 'plugin_motcle_list', 'add_plugin_motcle_list');
	add_submenu_page( 'plugin_carrousel_index', 'Ajouter', 'Ajouter', 'read', 'plugin_motcle_add', 'add_plugin_motcle_add');

	add_submenu_page( 'plugin_carrousel_index', 'Toutes les slides', 'Toutes les slides', 'read', 'plugin_slide_list', 'add_plugin_slide_list');
	add_submenu_page( 'plugin_carrousel_index', 'Ajouter', 'Ajouter', 'read', 'plugin_slide_add', 'add_plugin_slide_add');

}

function add_plugin_carrousel_index() {
    include('controller/index.php');
}
function add_plugin_motcle_add() {
    include('controller/add_motcle.php');
}
function add_plugin_motcle_list(){
	include('controller/list_motcle.php');
}
function add_plugin_slide_add() {
    include('controller/add_slide.php');
}

function add_plugin_slide_list(){
	include('controller/list_slide.php');
}

add_action( 'wp_enqueue_scripts', 'plugin_carrousel_style_front' );
function plugin_carrousel_style_front() {

    wp_enqueue_style( 'slides_style', plugin_dir_url(__FILE__).'slides.css' );
    wp_enqueue_script('slides', plugin_dir_url(__FILE__).'slides.js', array('jquery'));
}

add_shortcode('Urbi-carrousel', 'show_carrousel');


function show_carrousel(){

	$code = '';
	global $wpdb;
	$table1 = $wpdb->prefix . "slides";
	$table2 = $wpdb->prefix . "motcles";
	$sql = "select * from ".$table1 . ' s inner join ' .$table2. ' m on s.id_motcle = m.id_motcle order by ordre';
	$slides = $wpdb->get_results($sql, OBJECT);
	
	foreach($slides as $slide){
		$carrousel[$slide->titre][] = $slide;
	}
	$code .= '<div id="carrouselContainer">';
    $code .= '<div id="carrousel">';
	$code .= '<ul>';
	foreach($carrousel as $key => $slides){

		$slide = random_slide($slides);
		$code .= '<li><figure style="background: url('.$slide->src.') no-repeat; background-size:cover; background-position:center; height: 100% ; width: 100% ; margin: 0 ;">';

		// $code .= '<img alt="" src="'.$slide->src.'"/>';
		$code .= '<figcaption style="color:'.$slide->color.' ;">';

		if (get_locale() == 'en_GB') {
			$code .= '<span style="color:'.$slide->color.' ;">' . $slide->titre_en . '</span>';
			$code .= '<span style="color:'.$slide->color.' ;">' . $slide->caption_en. '</span>'  ;
		}else if(get_locale() == 'es_ES'){
			$code .= '<span style="color:'.$slide->color.' ;">' . $slide->titre_es . '</span>';
			$code .= '<span style="color:'.$slide->color.' ;">' . $slide->caption_es . '</span>';
		}else{
			$code .= '<span style="color:'.$slide->color.' ;">' . $slide->titre . '</span>';
			$code .= '<span style="color:'.$slide->color.' ;">' . $slide->caption . '</span>';
		}
		$code .= '</figcaption></figure>';
        $code .= '</li>';
	}
	$code .= '</ul>
			<div class="control">
				<button aria-hidden="true" id="playPauseCaroussel" class="pause" onclick="playPauseCaroussel()"> </button>
				<ul>
					<li class="selected" onclick="carousselGoOn(0)"></li>
					<li onclick="carousselGoOn(1)"></li>
					<li class="" onclick="carousselGoOn(2)"></li>
				</ul>
			</div>
	</div></div>';


	return $code;
}

function random_slide($slides){
    $size = count($slides);
    $random = rand(0, $size -1 );
    return $slides[$random];
}



