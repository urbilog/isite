<?php
    if(!isset($_GET['action'])){
        show_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_motcle.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_chiffre($_GET['motcle']);
        show_table();
    }

    function show_table(){
		$motcles = get_motcles();
		if(count($motcles) > 0){
	?>
			<div class="bootstrap-iso">
				<h1>Liste des mots clés</h1>
				<a href="?page=plugin_motcle_add">Ajouter un mot clé</a>
				<table class="table table-hover">
					<tr><th>Titre</th><th>Ordre</th><th>Actions</th></tr>
	<?php
			foreach($motcles as $motcle){
				echo '<tr>';
				echo '<td>'.$motcle->titre.'</td>';
				echo '<td>'.$motcle->ordre.'</td>';

				$delete = sprintf('<a class="carrousel_delete" value="'.$motcle->titre.'" href="?page=%s&action=%s&motcle=%s">Corbeille</a>',$_REQUEST['page'],'delete',$motcle->id_motcle);
				$edit = sprintf('<a href="?page=%s&action=%s&motcle=%s">Modifier</a>',$_REQUEST['page'],'edit',$motcle->id_motcle);

				echo '<td>'. $edit . ' | ' . $delete . '</td>';
				echo '</tr>';
			}
	?>
				</table>
			</div>
	<?php
		}else{
	?>
			<div class="wrap isite-chiffres">
				<h1>Liste des mots clés</h1>
					<p>Aucun mots clés disponible.</p>
			</div>
	<?php
		}
    }

    function get_motcles(){
        global $wpdb;
        $table = $wpdb->prefix . "motcles";
        $sql = "select * from " . $table;
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_chiffre($motcle){
        global $wpdb;
        $table = $wpdb->prefix . "motcles";
        $sql = "DELETE FROM " . $table. " WHERE id_motcle = ".$motcle;
        $wpdb->query($sql);
    }