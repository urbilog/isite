<?php
    function show_color($the_color,$color = array('red' => 'rouge', 'black' => 'noir', 'white' => 'blanc')){
        foreach($color as $key => $value){
            if($key == $the_color){
                echo '<option selected="selected" value="'.$key.'">'.$value.'</option>';
            }else{
                echo '<option value="'.$key.'">'.$value.'</option>';
            }
        }
    }

    function show_motcle($select){
        global $wpdb;
        $table = $wpdb->prefix . "motcles";
        $sql = "SELECT * FROM " . $table;
        $motcles = $wpdb->get_results($sql, OBJECT);
        foreach($motcles as $motcle){
            if($select == $motcle->id_motcle){
                echo '<option selected="selected" value="'.$motcle->id_motcle.'">'.$motcle->titre.'</option>';
            }else{
                echo '<option value="'.$motcle->id_motcle.'">'.$motcle->titre.'</option>';
            }
        }
    }

    function submit(){
        if(isset($_POST['slide_motcle']) && $_POST['slide_motcle'] != '' && $_POST['slide_src'] != '' && $_POST['slide_src'] != ''){
            echo '<div class="alert alert-success" role="alert">
                    <strong>Slide</strong> enregistrer avec succès !
                </div>';
            insert_slide($_POST['slide_motcle'], $_POST['slide_src'], $_POST['slide_color'], $_POST['slide_caption'], $_POST['slide_caption_en']);
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Veuillez compléter tous les champs.
                </div>';
        }
    }

    function insert_slide($motcle, $src, $color, $caption, $caption_en){
        global $wpdb;
        $table = $wpdb->prefix . "slides";
        $sql = "INSERT INTO " . $table . "(id_motcle, src, color, caption, caption_en) VALUES("
        .$motcle
        .",'".$src
        ."','".$color
        ."','".$caption
        ."','".$caption_en
        ."')";
        $wpdb->query($sql);
    }
?>
<div class="bootstrap-iso">
    <h1>Ajouter une slide</h1>
    <?php if(isset($_POST['add_slide'])){submit(); }?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="slide_motcle">Catégorie</label>
            <select name="slide_motcle" id="slide_motcle" value="<?php echo $_POST['slide_motcle']; ?>">
                <?php show_motcle($_POST['slide_motcle']); ?>
            </select>
        </div>
        <div class="form-group">
            <label for="slide_color" id="slide_color_label">Couleur</label>
            <input type="color" id="slide_color" name="slide_color" />
        </div>
        <div class="form-group">
            <label for="slide_caption">Légende française: </label>
            <input class="form-control" type="text" id="slide_caption" name="slide_caption" value="<?php echo $_POST['slide_caption']; ?>"/>
        </div>
        <div class="form-group">
            <label for="slide_caption_en">Légende anglaise: </label>
            <input class="form-control" type="text" id="slide_caption_en" name="slide_caption_en" value="<?php echo $_POST['slide_caption_en']; ?>"/>
        </div>


		<img id="slide_img" style="border: 1px solid #000; width: 500px; height: 200px;"/>
        <input type="hidden" name="slide_src" id="slide_src" value="<?php echo $_POST['slide_src']; ?>"/>

		<input type="button" value="Ajouter une image" name="add_slide_img" id="add_slide_img" class="btn btn-primary" />
        <input type="submit" name="add_slide" id="add_slide" value="Ajouter la slide" class="btn btn-primary" />
    </form>
</div>


