<?php
    if(!isset($_GET['action'])){
        show_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_slide.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_slide($_GET['slide']);
        show_table();
    }

    function show_table(){
		$slides = get_slides();
		if(count($slides) > 0){
	?>

			<div class="bootstrap-iso">
				<h1>Liste des slides du carrousel</h1>
				<a href="?page=plugin_slide_add">Ajouter une slide</a>
				<table class="table table-hover">
					<tr><th>Mot Clé</th><th>Ordre d\'affichage</th><th>Source</th><th>Couleur</th><th>Actions</th></tr>
	<?php
			foreach($slides as $slide){
				echo '<tr>';
				echo '<td>'.$slide->titre.'</td>';
				echo '<td>'.$slide->ordre.'</td>';
				echo '<td>'.$slide->src.'</td>';
				echo "<td style='background-color:$slide->color'></td>";

				$delete = sprintf('<a class="carrousel_delete" value="la slide" href="?page=%s&action=%s&slide=%s">Corbeille</a>',$_REQUEST['page'],'delete',$slide->id_slide);
				$edit = sprintf('<a href="?page=%s&action=%s&slide=%s">Modifier</a>',$_REQUEST['page'],'edit',$slide->id_slide);

				echo '<td>'. $edit . ' | ' . $delete . '</td>';
				echo '</tr>';
			}
	?>
				</table>
			</div>
	<?php
		}else{
	?>
			<div class="wrap isite-carrousel">
				<h1>Liste des slides du carrousel</h1>
				<p>Aucune slides disponibles.</p>
			</div>
	<?php
		}
    }

    function get_slides(){
        global $wpdb;
		$table1 = $wpdb->prefix . "slides";
		$table2 = $wpdb->prefix . "motcles";
		$sql = "select * from " . $table1. " s inner join ". $table2 . " m on s.id_motcle = m.id_motcle";
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_slide($slide){
        global $wpdb;
        $table = $wpdb->prefix . "slides";
        $sql = "DELETE FROM " . $table. " WHERE id_slide = ".$slide;
        $wpdb->query($sql);
    }