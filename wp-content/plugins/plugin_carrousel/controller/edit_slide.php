<?php
    function show_motcle($id_motcle){
        global $wpdb;
        $table = $wpdb->prefix . "motcles";
        $sql = "SELECT * FROM " . $table;
        $motcles = $wpdb->get_results($sql, OBJECT);
        foreach($motcles as $motcle){
            if($id_motcle == $motcle->id_motcle){
                echo '<option  selected="selected" value="'.$motcle->id_motcle.'">'.$motcle->titre.'</option>';
            }else{
                echo '<option value="'.$motcle->id_motcle.'">'.$motcle->titre.'</option>';
            }
        }
    }

    function show_color($the_color,$color = array('red' => 'rouge', 'black' => 'noir', 'white' => 'blanc')){
        foreach($color as $key => $value){
            if($key == $the_color){
                echo '<option selected="selected" value="'.$key.'">'.$value.'</option>';
            }else{
                echo '<option value="'.$key.'">'.$value.'</option>';
            }
        }
    }

    function update_slide($motcle, $src, $color, $caption, $caption_en, $caption_es){
        global $wpdb;
        $table = $wpdb->prefix . "slides";
        $sql = "UPDATE " . $table . " SET color='".$color."', caption='".$caption."', caption_en='".$caption_en."', caption_es='".$caption_es."',
        id_motcle=".$motcle.", src='".$src."' WHERE id_slide = " . $_GET['slide'];
        $wpdb->query($sql);

    }
?>
<div class="bootstrap-iso">
    <h1>Édition d'une slide</h1>
    <?php 
        if(isset($_POST['add_slide'])){
            if($_POST['slide_motcle'] != '' && $_POST['slide_src'] != ''){
                echo '<div class="alert alert-success" role="alert">
                    <strong>Modification </strong> enregistrée avec succès.
                </div>';
                update_slide($_POST['slide_motcle'], $_POST['slide_src'], $_POST['slide_color'], $_POST['slide_caption'], $_POST['slide_caption_en'], $_POST['slide_caption_es']);
            }else{
                echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Veillez à bien remplir tous les champs et ne pas oublier l\'image.
                </div>';
            }
        }

        if(isset($_GET['slide']) && $_GET['slide'] != ''){
            global $wpdb;
            $table = $wpdb->prefix . "slides";
            $sql = "SELECT * FROM ". $table. " WHERE id_slide = " . $_GET['slide'];
            $slide = $wpdb->get_results($sql, OBJECT);
            if(count($slide) >= 1){ $slide = $slide[0]; }else{
                echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Vous essayez de modifier du contenu qui n\'existe pas.
                </div>';
                die();
            }
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Vous essayez de modifier du contenu qui n\'existe pas.
                </div>';
        }
?>
    <form action="" method="POST">
        <div class="form-group">
            <select name="slide_motcle" class="form-control">
            <?php show_motcle($slide->id_motcle); ?>
            </select>
        </div>
        <div class="form-group">
            <label for="slide_color" id="slide_color_label">Couleur</label>
            <input type="color" name="slide_color" id="slide_color" class="f" value="<?php echo $slide->color ;?>"/>
        </div>
        <div class="form-group">
            <label for="slide_caption">Légende française: </label>
            <input class="form-control" type="text" id="slide_caption" name="slide_caption" value="<?php echo $slide->caption; ?>"/>
        </div>
        <div class="form-group">
            <label for="slide_caption_en">Légende anglaise: </label>
            <input class="form-control" type="text" id="slide_caption_en" name="slide_caption_en" value="<?php echo $slide->caption_en; ?>"/>
        </div>
        <div class="form-group">
            <label for="slide_caption_en">Légende espagnole: </label>
            <input class="form-control" type="text" id="slide_caption_es" name="slide_caption_es" value="<?php echo $slide->caption_es; ?>"/>
        </div>
		<img id="slide_img" src="<?php echo $slide->src ; ?>" style="border: 1px solid #000; width: 500px; height: 200px;"/>
        <input type="hidden" name="slide_src" id="slide_src" value="<?php echo $slide->src; ?>"/>
        <!-- <input type="hidden" name="slide_caption" id="slide_caption" value="<?php //echo $slide->caption; ?>"/> -->
		<input type="button" value="Ajouter une image" name="add_slide_img" id="add_slide_img" class="btn btn-primary" />
        <input type="submit" name="add_slide" id="add_slide" value="Modifier la slide" class="btn btn-primary" />
    </form>
</div>


