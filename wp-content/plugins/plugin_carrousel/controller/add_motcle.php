<?php

	if(isset($_POST['add_motcle'])){
		if(!empty($_POST['motcle_titre']) && !empty($_POST['motcle_ordre'])){
			global $wpdb;
			$table = $wpdb->prefix . "motcles";

			$sql = "INSERT INTO ". $table ."(titre, titre_en, titre_es, ordre) VALUES('"
			.htmlspecialchars($_POST['motcle_titre'],ENT_QUOTES)
			."','".htmlspecialchars($_POST['motcle_titre_en'],ENT_QUOTES)
			."','".htmlspecialchars($_POST['motcle_titre_es'],ENT_QUOTES)
			."',".$_POST['motcle_ordre'].")";

			$wpdb->query($sql);
			$msg = '<div class="alert alert-success" role="alert">
						<strong>Mot clé </strong> enregistré avec succès.
		  			</div>';
		}else{
			$msg = '<div class="alert alert-danger" role="alert">
						<strong>Attention !</strong> Veuillez remplir correctement tous les champs.
		  			</div>';
		}
	}
	else{
		
	}
?>

<div class="bootstrap-iso">
	<a href="?page=plugin_motcle_list">Les mots clés</a>
	<h1>Ajouter un mot clé</h1>
	<?php echo $msg ; ?>
    <form action="" method="POST">
		<div class="form-group">
			<label for="motcle_titre">Titre français: </label>
			<input type="text" name="motcle_titre" id="motcle_titre" class="form-control" placeholder="exemple: Titre1"  value="<?php echo $_POST['motcle_titre']; ?>" />
		</div>
		<div class="form-group">
			<label for="motcle_titre_en">Titre anglais: </label>
			<input type="text" name="motcle_titre_en" id="motcle_titre_en" class="form-control" placeholder="exemple: Titre1"  value="<?php echo $_POST['motcle_titre_en']; ?>" />
		</div>
		<div class="form-group">
			<label for="motcle_titre_en">Titre anglais: </label>
			<input type="text" name="motcle_titre_es" id="motcle_titre_es" class="form-control" placeholder="exemple: Titre1"  value="<?php echo $_POST['motcle_titre_es']; ?>" />
		</div>
		<div class="form-group">
			<label for="motcle_ordre">Ordre: </label>
			<input type="number" name="motcle_ordre" class="form-control" value="<?php echo $_POST['motcle_ordre']; ?>" placeholder="exemple: 1"/>
		</div>
        <input type="submit" name="add_motcle" id="add_motcle" value="Ajouter le mot clé" class="btn btn-primary" />
    </form>
</div>