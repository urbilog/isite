<?php
	$msg = '';
	if(isset($_POST['edit_motcle'])){
		global $wpdb;
        $table = $wpdb->prefix . "motcles";
        $sql = "UPDATE ". $table. " set titre = '"
        .$_POST['motcle_titre']
        ."', titre_en='".$_POST['motcle_titre_en']
        ."', titre_es='".$_POST['motcle_titre_es']."', ordre=".$_POST['motcle_ordre']." where id_motcle=".$_GET['motcle'];
        if($wpdb->query($sql));
        {
            $msg = '<div class="alert alert-success" role="alert">
                    <strong>Modification </strong> enregistrée avec succès !
                </div>';
        }
	}

	if(isset($_GET['motcle']) && $_GET['motcle'] != ''){
        global $wpdb;
        $table = $wpdb->prefix . "motcles";
        $sql = "SELECT * FROM ". $table. " WHERE id_motcle = " . $_GET['motcle'];
        $motcle = $wpdb->get_results($sql, OBJECT);
        if(count($motcle) >= 1){
			$motcle = $motcle[0];
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Vous essayez de modifier du contenu qui n\'existe pas !
                </div>';
			die();
        }
    }else{
        echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Vous essayez de modifier du contenu qui n\'existe pas !
                </div>';
		die();
    }

?>

<div class="bootstrap-iso">
	<h1>Édition mot clé</h1>
	<?php echo $msg ; ?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="motcle_titre">Titre français: </label>
            <input type="text" name="motcle_titre" id="motcle_titre" class="form-control" placeholder="exemple: Titre"  value="<?php echo $motcle->titre; ?>" />
        </div>
        <div class="form-group">
            <label for="motcle_titre_en">Titre anglais: </label>
            <input type="text" name="motcle_titre_en" id="motcle_titre_en" class="form-control" placeholder="exemple: Titre"  value="<?php echo $motcle->titre_en; ?>" />
        </div>
        <div class="form-group">
            <label for="motcle_titre_es">Titre espagnol: </label>
            <input type="text" name="motcle_titre_es" id="motcle_titre_es" class="form-control" placeholder="exemple: Titre"  value="<?php echo $motcle->titre_es; ?>" />
        </div>
        <div class="form-group">
            <label for="motcle_ordre">Ordre: </label>
            <input type="number" id="motcle_ordre" name="motcle_ordre"  class="form-control" value="<?php echo $motcle->ordre; ?>" placeholder="1"/>
        </div>
        <input type="submit" name="edit_motcle" id="edit_motcle" value="Modifier le mot clé" class="btn btn-primary" />
    </form>
</div>