var currentView = 0;
var timer= 7000;
var carousselInterval = null;
var pauseFromHover = false;
var pauseNotFromHover = false;

carousselInterval = setInterval(function(){carousselGoOn();}, timer);

function carousselGoOn(value){
    if(document.querySelector("#playPauseCaroussel") != null && document.querySelector("#playPauseCaroussel").getAttribute('class') == 'pause'){
      clearInterval(carousselInterval);
      carousselInterval=null;
      carousselInterval = setInterval(function(){carousselGoOn();}, timer);
    }
      
    if(value === undefined){
        value = currentView + 1;
    }
    if(document.querySelector('#carrousel ul:first-child') != undefined){
      value = value % (document.querySelector('#carrousel ul:first-child').children.length);

    }
    currentView = value;
    if(document.querySelector('#carrousel ul:first-child')){
      document.querySelector('#carrousel ul:first-child').style.left = "calc(-"+(value)+" * 100%)";
    }
    var bulles = document.querySelectorAll('#carrousel ul:last-child li');
    [].forEach.call(bulles, function(bulle) {
      bulle.classList.remove('selected');
    });
    if (bulles[value]) {
      bulles[value].classList.add('selected');
    }
}

function playPauseCaroussel(ele){
    if(carousselInterval){
        pauseCaroussel();
    }else{
        playCaroussel();
    }
}

function playCaroussel(){
  if(pauseFromHover || pauseNotFromHover){
    pauseFromHover = false;
    pauseNotFromHover = false;
    carousselInterval = setInterval(function(){carousselGoOn();}, timer);
    document.getElementById('playPauseCaroussel').classList.add('pause');
    document.getElementById('playPauseCaroussel').classList.remove('play');
  }
}

function pauseCaroussel(){
  if(!pauseFromHover){
    pauseNotFromHover=true;
  }
    clearInterval(carousselInterval);
    carousselInterval=null;
    document.getElementById('playPauseCaroussel').classList.add('play');
    document.getElementById('playPauseCaroussel').classList.remove('pause');
}
function playHoverCaroussel(){
  if(pauseFromHover){
    playCaroussel();
  }
}
function pauseHoverCaroussel(){
  if(!pauseNotFromHover){
    pauseFromHover=true;
    pauseCaroussel();
  }
}
