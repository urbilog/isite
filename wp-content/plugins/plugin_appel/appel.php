<?php
/*
Plugin Name: plugin-appel
Description: Gestion des appels à projets
Version: 0.2
Author: Urbilog
*/

	function create_taxonomy(){

	$labels = array(
	    'name' => __( 'Catégorie APP' ),
	    'singular_name' => __( 'Catégorie APP' ),
	    'all_items' => __('Toutes les catégories APP'),
	    'add_new_item' => __('Ajouter une catégorie APP'),
	    'edit_item' => __('Éditer la catégorie APP'),
	    'new_item' => __('Nouvelle catégorie APP'),
	    'view_item' => __('Voir les catégories APP'),
	    'search_items' => __('Rechercher parmi les catégories APP'),
	    'not_found' => __('Pas de catégorie APP trouvé '),
	    'not_found_in_trash'=> __('Pas de catégorie APP dans la corbeille')
	);

	$args = array(
	    'labels' => $labels,
	    'hierarchical' => true
	);
	register_taxonomy(
	    'app_category',
	    array( 'app' ),
	    $args
	);
	}
	add_action('init', 'create_taxonomy');

	function create_post_type() {

	$labels = array(
	    'name' => __( 'APP' ),
	    'singular_name' => __( 'APP' ),
	    'all_items' => __('Tous les APP'),
	    'add_new_item' => __('Ajouter un APP'),
	    'edit_item' => __('Éditer l\'APP'),
	    'new_item' => __('Nouveau APP'),
	    'view_item' => __('Voir l\'APP'),
	    'search_items' => __('Rechercher parmi les APP'),
	    'not_found' => __('Pas d\‘APP trouvé'),
	    'not_found_in_trash'=> __('Pas d\‘APP dans la corbeille')
	);

	$supports = array( 'title', 'editor' );
	$details = array(
	    'labels' => $labels,
	    'description' => '',
	    'public' => true,
	    'supports' => $supports,
	    'has_archive' => true,
	);
	register_post_type('app', $details);
	}

	add_action( 'init', 'create_post_type' );

	add_shortcode('Urbi-appel-cat', 'show_appel_cat');

	function show_appel_cat(){
		$args = array('hide_empty' => 0, 'hierarchical' => true, 'echo' => false, 'title_li' => '', 'taxonomy' => 'app_category', 'current_category' => 1, 'show_option_none'  => '',
		);
		return '<div id="filter" class="appels_a_projets"><ul>'.wp_list_categories($args).'</ul></div>';
	}




// register_activation_hook(__FILE__,'database_install_appels');
//
// register_deactivation_hook( __FILE__, 'database_delete_appels' );
//
// add_action('admin_menu', 'add_menu_plugin_appels');
//
// function database_install_appels() {
//
// 	global $wpdb;
//
// 	$table1 = $wpdb->prefix . "appels_a_projets_cat";
// 	$table2 = $wpdb->prefix . "appels_a_projets";
//
// 	$sql = "CREATE TABLE IF NOT EXISTS " . $table1 . "(id_appel_cat int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, libelle  TEXT, libelle_en TEXT, libelle_es TEXT) ENGINE=InnoDB";
// 	$wpdb->query($sql);
//
// 	$sql = "CREATE TABLE IF NOT EXISTS " . $table2 . " (id_appel int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, content TEXT, content_en TEXT, content_es TEXT, title TEXT, title_en TEXT, title_es TEXT, ordre int(10), clos TINYINT, lancement DATE, cloture DATE, id_appel_cat TEXT) ENGINE=InnoDB;";
// 	$wpdb->query($sql);
//
// 	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
// }
//
// add_action( 'admin_enqueue_scripts', 'plugin_style_appels' );
//
// function plugin_style_appels() {
// 	wp_enqueue_style( 'style_appels', plugin_dir_url(__FILE__).'style.css' );
// 	wp_enqueue_style( 'style_appels', plugin_dir_url(__FILE__).'bootstrap.urbilog.css' );
// 	wp_enqueue_script( 'script_appels', plugin_dir_url( __FILE__ ) . 'appel.js', array('jquery'), '1.0' );
//
// }
//
// function database_delete_appels(){
//
// }
//
// function add_menu_plugin_appels(){
// 	add_menu_page( 'appels_a_projets', 'Appels à projets', '', 'plugin_appels_a_projets_index', 'add_plugin_appels_index','
// 	dashicons-controls-volumeon' );
// 	add_submenu_page( 'plugin_appels_a_projets_index', 'Tous appels à projets', 'Tous appels à projets', 'read', 'plugin_appels_a_projets_list', 'plugin_appels_a_projets_list');
// 	add_submenu_page( 'plugin_appels_a_projets_index', 'Ajouter un appel à projet', 'Ajouter un appel à projet', 'read', 'plugin_appels_a_projets_add', 'plugin_appels_a_projets_add');
// 	add_submenu_page( 'plugin_appels_a_projets_index', 'Ajouter une nouvelle catégorie', 'Ajouter une nouvelle catégorie', 'read', 'plugin_appels_a_projets_cat_add','plugin_appels_a_projets_cat_add');
// 	add_submenu_page( 'plugin_appels_a_projets_index', 'Toutes les catégories d\'appels', 'Toutes les catégories d\'appels', 'read', 'plugin_appels_a_projets_cat_list','plugin_appels_a_projets_cat_list');
//
// }
//
// function add_plugin_appels_index() {
//     include('controller/index.php');
// }
// function plugin_appels_a_projets_add() {
//     include('controller/add_appels_a_projets.php');
// }
// function plugin_appels_a_projets_list() {
//     include('controller/list_appels_a_projets.php');
// }
// function plugin_appels_a_projets_edit(){
// 	include('controller/edit_appels_a_projets.php');
// }
// function plugin_appels_a_projets_cat_add(){
// 	include('controller/add_appels_a_projets_cat.php');
// }
// function plugin_appels_a_projets_cat_edit(){
// 	include('controller/edit_appels_a_projets_cat.php');
// }
// function plugin_appels_a_projets_cat_list(){
// 	include('controller/list_appels_a_projets_cat.php');
// }
// add_shortcode('Urbi-appel-cat', 'show_cat_appel');
//
// function show_cat_appel(){
// 	$return = '';
// 	if(isset($_REQUEST['cat']) && $_REQUEST['cat'] !=''){
// 		$cat = $_REQUEST['cat'];
// 	}
//
//
// 	$LANG = appel_get_term_lang();
//
//
// 	global $wpdb;
//
// 	$table = $wpdb->prefix . "appels_a_projets";
// 	$table1 = $wpdb->prefix. "appels_a_projets_cat";
// 	$select = "";
// 	$current = get_locale();
//
// 	switch($current){
// 		case "en_GB" :
// 			$select = ' id_appel_cat, libelle_en as libelle ';
// 		break;
// 		case "es_ES" :
// 			$select = ' id_appel_cat, libelle_es as libelle ';
// 		break;
// 		case "fr_FR" :
// 			$select = ' id_appel_cat, libelle as libelle ';
// 		break;
// 	}
//
// 	$sql = "SELECT " . $select . " FROM " . $table1  ;
// 	$categories = $wpdb->get_results($sql, OBJECT);
//
// 	$sql = "SELECT " . $select . " FROM " . $table1 . " WHERE id_appel_cat = " . $cat ;
// 	$c = $wpdb->get_results($sql, OBJECT);
//
//
// 	$return .= '<div id="filter" class="appels_a_projets">';
// 	$return .= '<ul>';
//
// 	if(count($c) != 0){
// 		$return .= '<li class=""><a href="' . $LANG->APPEL_LIEN.'" title="'.$LANG->APPEL_LIEN_TITRE_TOUS.'" class="link_app">'.$LANG->APPEL_LIEN_LIBELLE.'</a></li>';
// 	} else {
// 		$return .= '<li class=""><a href="'.$LANG->APPEL_LIEN.'" title="'.$LANG->APPEL_LIEN_TITRE_TOUS.'" class="link_app">'.$LANG->APPEL_LIEN_LIBELLE.'</a></li>';
// 	}
//
// 	foreach($categories as $cat){
// 		if($cat->id_appel_cat == $c[0]->id_appel_cat){
// 			$return .= '<li class="current-cat"><a href="#" class="link_app" data-id="'.$cat->libelle.'">'.$cat->libelle.'</a></li>';
// 		}else{
// 			$return .= '<li><a href="'.$LANG->APPEL_LIEN .'?cat='.$cat->id_appel_cat.'" class="link_app" data-id="'.$cat->libelle.'">'.$cat->libelle.'</a></li>';
// 		}
// 	}
// 	$return .= '</ul>';
// 	$return .= '</div>';
// 	return $return;
//
// }
//
// add_shortcode('Urbi-appel', 'show_appels_a_projets');
//
// function show_appels_a_projets($cat = 0){
// 	if(isset($_REQUEST['cat'])){
// 		$cat = $_REQUEST['cat'];
// 	}
//
// 	$LANG = appel_get_term_lang();
// 	$current = get_locale();
//
// 	global $wpdb;
// 	$return = show_cat_appel($cat);
// 	$table = $wpdb->prefix . "appels_a_projets";
//
// 	$current = get_locale();
// 	$select = "";
//
// 	switch($current){
// 		case "en_GB" :
// 			$select = ' id_appel, content_en as content, title_en as title, lancement, cloture, id_appel_cat ';
// 			break;
// 		case "es_ES" :
// 			$select = ' id_appel, content_es as content, title_es as title, lancement, cloture, id_appel_cat ';
// 			break;
// 		case "fr_FR" :
// 			$select = ' id_appel, content as content, title as title, lancement, cloture, id_appel_cat ';
// 			break;
// 	}
//
// 	$sql = "SELECT " . "*" . " FROM " . $wpdb->prefix."appels_a_projets_cat" . " WHERE id_appel_cat = " . $cat ;
// 	$c = $wpdb->get_results($sql, OBJECT);
//
// 	if(count($c) == 0){
// 		$sql = "SELECT ". $select ." FROM " . $table . " where cloture > NOW() order by lancement DESC";
// 		$appels = $wpdb->get_results($sql, OBJECT);
// 		$sql = "SELECT ". $select." FROM " . $table . " where cloture < NOW() order by lancement DESC";
// 		$appels = array_merge($appels,$wpdb->get_results($sql, OBJECT));
//
// 	}else{
// 		$libelle = $c[0]->libelle;
// 		$sql = "SELECT ".$select." FROM " . $table . " where cloture > NOW() AND id_appel_cat LIKE '%|".$libelle."|%' order by lancement DESC";
// 		$appels = $wpdb->get_results($sql, OBJECT);
// 		$sql = "SELECT ".$select." FROM " . $table . " where cloture < NOW() AND id_appel_cat LIKE '%|".$libelle."|%' order by lancement DESC";
// 		$appels = array_merge($appels,$wpdb->get_results($sql, OBJECT));
//
// 	}
//
// 	$return .= '<div class="les_appels">';
//
// 	foreach($appels as $appel){
//
// 		$date = new DateTime($appel->cloture);
// 		$date_debut = new DateTime($appel->lancement);
// 		$current_date = new DateTime();
// 		if($date_debut <= $current_date){
// 			if($current_date < $date){
// 				$return .= "<div class='".$LANG->APPEL_CLASSE_OUVERT."'>";
// 			}else{
// 				$return .= "<div class='".$LANG->APPEL_CLASSE_FERME."'>";
// 			}
//
// 			$return .= "<h3>".$appel->title."</h3>";
// 			$return .= wpautop($appel->content);
// 			$return .= "</div>";
// 		}
//
// 	}
// 	$return .= '</div>';
// 	return $return;
// }
//
// function appel_get_term_lang(){
//     $lang = get_bloginfo("language");
//     $domaine = get_bloginfo("wpurl");
//     $file = "fr.json" ;
//     $rep  = "lang" ;
//     switch ($lang) {
//         case "fr-FR": $file = "fr.json" ;
//             break;
//         case "en-GB": $file = "en.json" ;
// 			break;
// 		case "es-ES": $file = "es.json";
// 			break;
//     }
//     $content = wp_remote_get($domaine.'/wp-content/plugins/plugin_appel/lang/'.$file);
//     return json_decode($content["body"]);
// }
