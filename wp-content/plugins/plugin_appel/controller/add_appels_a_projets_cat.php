<h1>Nouvel appel à projet</h1>
<div class="bootstrap-iso" id="isite-appels">
    <?php if (isset($_POST['appel_add_submit'])){ ?>

    <div class="alert alert-success" role="alert">
        <strong>Succès !</strong> Catégorie d'appel à projet enregistrer avec succès !
    </div>
    <?php } ?>
</div>
    <form method="post" action="" id="add_appel_a_projet">
        <div class="form-group">
            <label for="appel_libelle">Libelle français</label>
            <input class="form-control" type="text" name="appel_libelle" id="appel_libelle" required/>
        </div>
        <div class="form-group">
            <label for="appel_libelle_en">Libelle anglais</label>
            <input class="form-control" type="text" name="appel_libelle_en" id="appel_libelle_en" required/>
        </div>
        <div class="form-group">
            <label for="appel_libelle_en">Libelle espagnol</label>
            <input class="form-control" type="text" name="appel_libelle_es" id="appel_libelle_es" required/>
        </div>

        <input type="submit" value="enregistrer" name="appel_add_submit" id="appel_add_submit" class="button button-primary"/>
    </form>
</div>
<?php 

    if(isset($_POST['appel_libelle'])){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets_cat";
        $sql = "INSERT INTO ". $table ."(libelle, libelle_en, libelle_es) VALUES('"
        .htmlspecialchars($_POST['appel_libelle'],ENT_QUOTES)
        ."','".$_POST['appel_libelle_en']
        ."','".$_POST['appel_libelle_es'].
        "')";
        $wpdb->query($sql);
    }

