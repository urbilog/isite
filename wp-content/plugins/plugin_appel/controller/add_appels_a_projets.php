<h1>Nouvel appel à projet</h1>
<div class="bootstrap-iso" id="isite-appels">
    <?php if(isset($_POST['appel_add_submit'])){ ?>
        <div class="alert alert-success">
            <strong>Succès !</strong> Appel à projet enregistré avec succès !
        </div>
    <?php } ?>
    <form method="post" action="" id="add_appel_a_projet">

        <div class="form-group">
            <label for="appel_title">Titre français</label>
            <input class="form-control" type="text" name="appel_title" id="appel_title" required value="<?php echo $_POST['appel_title']; ?>"/>
        </div>
        <div class="form-group">
            <label for="appel_title_en">Titre anglais</label>
            <input class="form-control" type="text" name="appel_title_en" id="appel_title_en" required value="<?php echo $_POST['appel_title_en']; ?>"/>
        </div>
        <div class="form-group">
            <label for="appel_title_en">Titre espagnol</label>
            <input class="form-control" type="text" name="appel_title_es" id="appel_title_es" required value="<?php echo $_POST['appel_title_es']; ?>"/>
        </div>
        <div class="form-check">
            <?php show_appels_a_projets_cat($_POST['appel_cat']); ?>
        </div>
        <br>
        <div class="form-group">
            <label for="appel_content">Contenu français</label>
        <?php 
            $content = $_POST['appel_content'];
            $editor_id = 'appel_content';
            wp_editor( $content, $editor_id ); 
        ?>
        </div>
        <div class="form-group">
        <label for="appel_content_en">Contenu anglais</label>
        <?php 
            $content = $_POST['appel_content_en'];
            $editor_id = 'appel_content_en';
            wp_editor( $content, $editor_id ); 
        ?>
        </div>
        <div class="form-group">
        <label for="appel_content_es">Contenu espagnol</label>
        <?php 
            $content = $_POST['appel_content_es'];
            $editor_id = 'appel_content_es';
            wp_editor( $content, $editor_id ); 
        ?>
        </div>
        <label for="appel_lancement">Date de lancement: </label><input class="form-control" type="date" name="appel_lancement" id="appel_lancement" required value="<?php echo $_POST['appel_lancement']; ?>">
        <label for="appel_cloture">Date de cloture: </label><input class="form-control" type="date" name="appel_cloture" id="appel_cloture" required value="<?php echo $_POST['appel_cloture']; ?>">
        <input type="submit" value="enregistrer" name="appel_add_submit" id="appel_add_submit" class="btn btn-primary"/>
    </form>
</div>
<?php 

    if(isset($_POST['appel_content']) && isset($_POST['appel_title']) && isset($_POST['appel_lancement']) && isset($_POST['appel_cloture'])){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets";
        $insert = '|';
        foreach($_POST['appel_cat'] as $cat){
            $insert .= $cat.'|';
        }
        $sql = "INSERT INTO ". $table ."(title, title_en, title_es, content, content_en, content_es, lancement, cloture, id_appel_cat) VALUES('"
        .htmlspecialchars($_POST['appel_title'],ENT_QUOTES)
        ."','".htmlspecialchars($_POST['appel_title_en'],ENT_QUOTES)
        ."','".htmlspecialchars($_POST['appel_title_es'],ENT_QUOTES)
        ."','".wpautop($_POST['appel_content'])
        ."','".wpautop($_POST['appel_content_en'])
        ."','".wpautop($_POST['appel_content_es'])
        ."', '".$_POST['appel_lancement']
        ."', '".$_POST['appel_cloture']
        ."', '".$insert."')";
        $wpdb->query($sql);

    }

    function show_appels_a_projets_cat($select){

        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets_cat";
        $sql = "SELECT * FROM " . $table;
        $cats = $wpdb->get_results($sql, OBJECT);
        foreach($cats as $cat){
            echo '<label for="appel_cat_'.$cat->id_appel_cat.'">'.$cat->libelle.'</label>';
            echo '<input class="form-check-input" type="checkbox" name="appel_cat[]" id="appel_cat_'.$cat->id_appel_cat.'" value="'.$cat->libelle.'"/>'; 
        }
    }

