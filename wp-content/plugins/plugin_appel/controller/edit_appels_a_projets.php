<?php
        function show_appel_a_projet_cat($cats_checked){
                 global $wpdb;
                $table = $wpdb->prefix . "appels_a_projets_cat";
                $sql = "SELECT * FROM " . $table;
                $cats = $wpdb->get_results($sql, OBJECT);
                $bool = false;
                foreach($cats as $cat){
                    foreach($cats_checked as $checked){
                        if($cat->libelle == $checked){
                            echo '<label class="form-check-label" for="appel_cat_'.$cat->id_appel_cat.'">'.$cat->libelle.'</label>';
                            echo '<input class="form-check-input" type="checkbox" checked name="appel_cat[]" id="appel_cat_'.$cat->id_appel_cat.'" value="'.$cat->libelle.'"/>'; 
                            $bool = true;
                        }
                    }

                    if(!$bool){
                        echo '<label class="form-check-label" for="appel_cat_'.$cat->id_appel_cat.'">'.$cat->libelle.'</label>';
                        echo '<input class="form-check-input" type="checkbox" name="appel_cat[]" id="appel_cat_'.$cat->id_appel_cat.'" value="'.$cat->libelle.'"/>'; 
                    }

                    $bool = false;
                }
        }
    
    function update_appel($content, $content_en, $title, $title_en, $lancement, $cloture, $cat, $title_es, $content_es){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets";
        $sql = "UPDATE " . $table . " SET content='".wpautop($content)
        ."', content_en='".$content_en
        ."', content_es='".$content_es
        ."', title='".$title
        ."', title_en='".$title_en
        ."', title_es='".$title_es
        ."', lancement='".$lancement.
        "', cloture='".$cloture.
        "', id_appel_cat='".$cat."' WHERE id_appel = " . $_GET['appel'];
        $wpdb->query($sql);
    }
?>
<div class="bootstrap-iso" id="isite-appels">
    <?php 
        if(isset($_POST['appel_edit_submit'])){
            if(isset($_POST['appel_title']) && isset($_POST['appel_content'])  && isset($_POST['appel_lancement'])  && isset($_POST['appel_cloture'])){ ?>
                <div class="alert alert-success" role="alert">
                    <strong>Succès !</strong> Modification enregistrée !
                </div>
        <?php   
                // var_dump('coucou');
                $insert = '';
                if($_POST['appel_cat'] != null){
                    $insert = '|';
                    foreach($_POST['appel_cat'] as $cat){
                        $insert .= $cat.'|';
                    }
                }
                
                // var_dump($insert);
                update_appel($_POST['appel_content'], $_POST['appel_content_en'], $_POST['appel_title'], $_POST['appel_title_en'],$_POST['appel_lancement'], $_POST['appel_cloture'], $insert, $_POST['appel_title_es'], $_POST['appel_content_es']);
            }
            
        }

        if(isset($_GET['appel']) && $_GET['appel'] != ''){
            global $wpdb;
            $table = $wpdb->prefix . "appels_a_projets";
            $sql = "SELECT * FROM ". $table. " WHERE id_appel = " . $_GET['appel'];
            $appel = $wpdb->get_results($sql, OBJECT);
            if(count($appel) >= 1){ $appel = $appel[0]; }else{ ?>
                <div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Vous essayez de mofidier du contenu qui n'existe pas !
                </div>
            <?php die(); }
        }else{ ?>
            <div class="alert alert-danger" role="alert">
                <strong>Attention !</strong> Vous essayez de mofidier du contenu qui n'existe pas !
            </div>   
        <?php }
    ?>
<h1>Modifier l'appel à projet</h1>
<div class="bootstrap-iso">
    <form method="post" action="" id="add_appel_a_projet">
        <div class="form-group">
            <label for="appel_title">Titre français: </label>
            <input class="form-control" type="text" name="appel_title" id="appel_title" value="<?php echo $appel->title; ?>" required/>
        </div>
        <div class="form-group">
            <label for="appel_title_en">Titre anglais: </label>
            <input class="form-control" type="text" name="appel_title_en" id="appel_title_en" value="<?php echo $appel->title_en; ?>" required/>
        </div>
        <div class="form-group">
            <label for="appel_title_es">Titre espagnol: </label>
            <input class="form-control" type="text" name="appel_title_es" id="appel_title_es" value="<?php echo $appel->title_es; ?>" required/>
        </div>
        <div class="form-check">
            <?php 
                $cats = explode('|', $appel->id_appel_cat);
                show_appel_a_projet_cat($cats); ?>
        </div>
        <?php 
            $content = $appel->content;
            $editor_id = 'appel_content'; ?>
            <label for='appel_content'>Contenu français: </label>
        <?php 
            wp_editor( $content, $editor_id ); 

            $content = $appel->content_en;
            $editor_id = 'appel_content_en'; ?>
        <label for="appel_content_en">Contenu anglais: </label>
        <?php wp_editor( $content, $editor_id ); ?>

        <?php 
            $content = $appel->content_es;
            $editor_id = 'appel_content_es'; ?>
            <label for="appel_content_en">Contenu espagnol: </label>
            <?php wp_editor( $content, $editor_id ); ?>

        <div class="form-group">
            <label for="appel_lancement">Date de lancement: </label>
            <input class="form-control" type="date" name="appel_lancement" id="appel_lancement" value="<?php echo $appel->lancement; ?>" required>
        </div>
        <div class="form-group">
            <label for="appel_cloture">Date de cloture: </label>
            <input class="form-control" type="date" name="appel_cloture" id="appel_cloture" value="<?php echo $appel->cloture; ?>" required>
        </div>

        <input type="submit" value="Modifier" name="appel_edit_submit" class="btn btn-primary" id="appel_add_submit"/>
    </form>
</div>


