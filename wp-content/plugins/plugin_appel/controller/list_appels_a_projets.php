<?
    if(!isset($_GET['action'])){
        show_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_appels_a_projets.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_appels_a_projet($_GET['appel']);
        show_table();
    }

    function show_table(){

            $current = strtotime(date('Y-m-d'));
            $appels = get_appels_a_projets(); ?>

            <div class="bootstrap-iso">
                <h1>Liste des appels à projets</h1>
                <a href="?page=plugin_appels_a_projets_add" title="Vers formulaire d'ajout d'un appel à projet">Ajouter un nouvel appel à projet</a>

            <? if(count($appels) > 0){ ?>
                
                <table class="table table-hover">
                    <tr>
                        <th>Titre</th>
                        <th>État</th>
                        <th>Date de lancement</th>
                        <th>Date de cloture</th>
                        <th>Catégorie</th>
                        <th>Actions</th>
                    </tr>

            <? foreach($appels as $appel){ ?>

                    <tr>
                        <td><? echo $appel->title; ?></td>
                        
            <? if(strtotime($appel->cloture) < $current){ ?>
                        <td>Clos</td>
            <? }else{ ?>
                        <td>En cours</td>
            <? }
                
                $delete = sprintf('<a class="partenaires_delete" href="?page=%s&action=%s&appel=%s" value="%s" class="appels_delete">Corbeille</a>',$_REQUEST['page'],'delete',$appel->id_appel, $appel->title);
                $edit = sprintf('<a href="?page=%s&action=%s&appel=%s">Modifier</a>',$_REQUEST['page'],'edit',$appel->id_appel);
            ?>
                        <td> <? echo $appel->lancement; ?> </td>
                        <td> <? echo $appel->cloture; ?> </td>
                        <td> <? echo $appel->id_appel_cat; ?> </td>
                        <td> <? echo $edit; ?> | <? echo $delete; ?> </td>
                    </tr>
            <? } ?>
                </table>
        <? }else{ ?>
                <p>Aucun appel à projet disponible.</p>
        <? } ?>
            </div>
        <?
    }

    function get_appels_a_projets(){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets";
        $sql = "select * from " . $table;
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_appels_a_projet($appel){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets";
        $sql = "DELETE FROM " . $table. " WHERE id_appel = ".$appel;
        $wpdb->query($sql);
    }