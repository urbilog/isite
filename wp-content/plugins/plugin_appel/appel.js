jQuery(document).ready(function($){
    
    var eles = $('.appels_delete');

    eles.each(function(){
        $(this).click(function(event){
            if(confirm("Êtes vous sur de vouloir supprimer '" + $(this).attr('value')+ "' ?")){
            }else{
                event.preventDefault();
            }
        });
    });


    //var regex = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/;


    $('#appel_add_submit').click(function(){
        var empty = new Array();
        var correctDate = new Array();
        var form = this.parentNode;
        var inputs = form.querySelectorAll('[required]');
        var dates  = form.querySelectorAll('input[type=date]');
        var labels = form.querySelectorAll('label');
        var container = form.parentNode;
        // var checkbox = document.querySelectorAll('input[type=checkbox].form-check-input');
        // var checked = false;
        // console.log(checkbox);
        // for(var i = 0 ; i < checkbox.length ; i++){
        //     if(checkbox[i].checked){
        //         checked = true;
        //     }
        // }

        // if(!checked){
        //     return false;
        // }
        var msg = 'Veuillez remplir correctement les champs suivants: ';
        var error = '';
        error += '<div class="error alert" role="alert">'
        error += '<strong>Attention !</strong> ';



        for(var i = 0 ; i < inputs.length ; i ++){
            if(inputs[i].value == ''){
                empty.push(labels[i].textContent.replace(':',''));
            }
        }

        for(var i = 0 ; i < empty.length ; i ++){
            if(i != empty.length - 1){
                msg += empty[i].replace(':','') + ', ';
            }else{
                msg += empty[i].replace(':','') + '.';
            }
        }


        if(empty.length > 0){
            error += msg;
            error += '</div>'
            container.innerHTML = error + container.innerHTML;
            event.preventDefault();

        }else{

            var start = new Date(dates[0].value);
            var end   = new Date(dates[1].value);

            if(end < start){
                msg = 'La date de cloture ne peut pas être antérieure à la date de lancement.';
                error += msg ;
                error += '</div>';

                container.innerHTML = error + container.innerHTML;
                event.preventDefault();

            }
        }
    });

});
