<?php

    function update_appel_cat($libelle, $libelle_en, $libelle_es){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets_cat";
        $sql = "UPDATE " . $table . " SET libelle='".$libelle."', 
        libelle_en='".$libelle_en."', libelle_es='".$libelle_es."' WHERE id_appel_cat = " . $_GET['appel_cat'];
        $wpdb->query($sql);
    }
?>
<div class="bootstrap-iso" id="isite-appels">
    <?php 
        if(isset($_POST['appel_edit_submit'])){
            if($_POST['appel_libelle']){ ?>
                <div class="alert alert-success" role="alert">
                    <strong>Succès !</strong> Modification enregistrée !
                </div>
        <?php   update_appel_cat($_POST['appel_libelle'], $_POST['appel_libelle_en'],$_POST['appel_libelle_es']);
            }
        }

        if(isset($_GET['appel_cat']) && $_GET['appel_cat'] != ''){
            global $wpdb;
            $table = $wpdb->prefix . "appels_a_projets_cat";
            $sql = "SELECT * FROM ". $table. " WHERE id_appel_cat = " . $_GET['appel_cat'];
            $appel_cat = $wpdb->get_results($sql, OBJECT);
            if(count($appel_cat) >= 1){ $appel_cat = $appel_cat[0]; }else{ ?>
                <div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Vous essayez de mofidier du contenu qui n'existe pas !
                </div>
            <?php die(); }
        }else{ ?>
            <div class="alert alert-danger" role="alert">
                <strong>Attention !</strong> Vous essayez de mofidier du contenu qui n'existe pas !
            </div>   
        <?php die ();}
    ?>

<h1>Modifier catégorie d'appel à projet</h1>
<div class="bootsrap-iso">
    <form method="post" action="" id="add_appel_a_projet">
        <div class="form-group">
            <label for="appel_libelle">Libelle français</label>
            <input class="form-control" type="text" name="appel_libelle" id="appel_libelle"  required value="<?php echo $appel_cat->libelle; ?>"/>
        </div>

        <div class="form-group">
            <label for="appel_libelle_en">Libelle anglais</label>
            <input class="form-control" type="text" name="appel_libelle_en" id="appel_libelle_en"  required value="<?php echo $appel_cat->libelle_en; ?>"/>
        </div>

        <div class="form-group">
            <label for="appel_libelle_en">Libelle espagnol</label>
            <input class="form-control" type="text" name="appel_libelle_es" id="appel_libelle_es"  required value="<?php echo $appel_cat->libelle_es; ?>"/>
        </div>
        <input type="submit" value="Modifier" name="appel_edit_submit" id="appel_add_submit" class="btn btn-primary"/>
    </form>
</div>


