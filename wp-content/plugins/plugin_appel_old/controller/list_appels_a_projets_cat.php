<?
    if(!isset($_GET['action'])){
        show_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_appels_a_projets_cat.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_appels_a_projet($_GET['appel_cat']);
        show_table();
    }

    function show_table(){

            $current = strtotime(date('Y-m-d'));
            $appels_cat = get_appels_a_projets(); ?>

            <div class="wrap bootstrap-iso">
                <h1>Liste des catégories appels à projets</h1>
                <a href="?page=plugin_appels_a_projets_cat_add" title="Vers formulaire d'ajout d'une catégorie d'appel à projet">Ajouter une nouvelle catégorie d'appel à projet</a>

            <? if(count($appels_cat) > 0){ ?>
                
                <table class="table table-hover">
                    <tr>
                        <th>Libelle</th>
                        <th>Actions</th>
                    </tr>

            <? foreach($appels_cat as $appel_cat){ ?>

                    <tr>
                        <td><? echo $appel_cat->libelle; ?></td>
            <?php
                $delete = sprintf('<a href="?page=%s&action=%s&appel_cat=%s" value="%s" class="appels_delete">Corbeille</a>',$_REQUEST['page'],'delete',$appel_cat->id_appel_cat, $appel_cat->libelle);
                $edit = sprintf('<a href="?page=%s&action=%s&appel_cat=%s">Modifier</a>',$_REQUEST['page'],'edit',$appel_cat->id_appel_cat);
            ?>
                        <td> <? echo $edit; ?> | <? echo $delete; ?> </td>
                    </tr>
            <? } ?>
                </table>
        <? }else{ ?>
                <p>Aucune catégorie d'appel à projet disponible.</p>
        <? } ?>
            </div>
        <?
    }

    function get_appels_a_projets(){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets_cat";
        $sql = "select * from " . $table;
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_appels_a_projet($appel_cat){
        global $wpdb;
        $table = $wpdb->prefix . "appels_a_projets_cat";
        $sql = "DELETE FROM " . $table. " WHERE id_appel_cat = ".$appel_cat;
        $wpdb->query($sql);
    }