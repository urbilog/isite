<?php
/*
Plugin Name: plugin-actus
Description: Affichage des dernières actualités
Version: 0.1
Author: Urbilog
*/

add_shortcode('Urbi-actus', 'show_actus');

function show_actus(){ ?>
	<div id="lastNews" class="filterContainer">
			<?php
			if(get_bloginfo("language") == 'en-GB'){ ?>
				<h2> Last News </h2>
			<?php }else if(get_bloginfo("language") == 'es'){ ?>
				<h2> Últimas noticias </h2>
			<?php }else{ ?>
				<h2> Dernières actualités </h2>
			<?php } ?>
			<ul class="newsList list">
				<?php query_posts('posts_per_page=3'); ?>
				<?php while (have_posts()) : the_post(); ?>
					<li>
						<article id="post-<?php the_ID(); ?>">
							<a href="<?php the_permalink('',get_the_ID()); ?>" title="<?php the_title();?>">
								<div class="thumbnailContainer article">
									<div class="color color6"></div>
									<img class="thumbnail article" src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""/>
								</div>
								<h3><?php the_title();?></h3>
								<?php $content = get_the_content();
								$content = preg_replace("/\[caption.*\[\/caption\]/", '', $content);
								$content = apply_filters('the_content', $content); ?>
								<p><?php echo wp_trim_words($content, 34, '(...)');?></p>
							</a>
						</article>
					</li>
				<?php endwhile;?>
			</ul>
			<div class="navigation_publi">
			<?php
		if(get_bloginfo("language") == 'en-GB'){ ?>
			<a href="/index.php/en/news/"><p>All our news</p></a>
		<?php }else if(get_bloginfo("language") == 'es'){ ?>
			<a href="/index.php/es/noticias/"><p>Todas nuestras noticias</p></a>
		<?php  }else{ ?>
			<a href="/index.php/fr/actualites-2/"><p>Toutes nos actualités</p></a>
		<?php } ?>
			</div>
	</div>

<?php } ?>
