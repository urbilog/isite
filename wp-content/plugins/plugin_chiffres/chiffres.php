<?php
/*
Plugin Name: plugin-chiffres
Description: Gestion des partenaires
Version: 0.1
Author: Urbilog
*/

ini_set("display_errors", 1);

register_activation_hook(__FILE__,'chiffres_database_install');

// register_deactivation_hook( __FILE__, 'database_delete' );

add_action('admin_menu', 'add_menu_plugin_chiffres');

function chiffres_database_install() {

	global $wpdb;

	$table = $wpdb->prefix . "chiffres";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table . " (id_chiffre int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, valeur TEXT, description TEXT, description_en TEXT, description_es TEXT, ordre int(10), src TEXT, url TEXT, url_en TEXT, url_es TEXT) ENGINE=InnoDB";
	$wpdb->query($sql);

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
}

add_action( 'admin_enqueue_scripts', 'plugin_chiffres_style' );

function plugin_chiffres_style() {
	wp_enqueue_style( 'chiffres_style', plugin_dir_url(__FILE__).'chiffres.css' );
	wp_enqueue_script( 'chiffres_script', plugin_dir_url( __FILE__ ) . 'chiffres.js', array(), '1.0' );
	wp_enqueue_style( 'chiffres_bootstrap', plugin_dir_url(__FILE__).'bootstrap.urbilog.css');

}

function add_menu_plugin_chiffres(){
	add_menu_page( 'Chiffres clés', 'Chiffres clés', '', 'plugin_chiffres_index', 'add_plugin_partenaires_index','dashicons-chart-area' );
	add_submenu_page( 'plugin_chiffres_index', 'Tous les chiffres clés', 'Tous les chiffres clés', 'read', 'plugin_chiffres_list', 'add_plugin_chiffres_list');
	add_submenu_page( 'plugin_chiffres_index', 'Ajouter un chiffre clé', 'Ajouter un chiffre clé', 'read', 'plugin_chiffres_add', 'add_plugin_chiffres_add');
}

function add_plugin_chiffres_index() {
    include('controller/index.php');
}
function add_plugin_chiffres_add() {
    include('controller/add_chiffres.php');
}

function add_plugin_chiffres_list(){
	include('controller/list_chiffres.php');
}



add_shortcode('Urbi-chiffres', 'show_chiffres');



function show_chiffres(){


	global $wpdb;
	$table = $wpdb->prefix . "chiffres";


	$current = get_bloginfo("language");
	$LANG = chiffre_get_term_lang();
	$select = "";
	$return = "";

	switch($current){
		case "en-GB" :
			$select = ' id_chiffre, valeur, description_en as description, ordre, src, url_en as url ';
			break;
		case "es" :
			$select = ' id_chiffre, valeur, description_es as description, ordre, src, url_es as url ';
			break;
		case "fr-FR" :
			$select = ' id_chiffre, valeur, description as description, ordre, src, url as url ';
			break;
	}
	$sql = "select ". $select . " from ".$table . ' order by ordre';
	$chiffres = $wpdb->get_results($sql, OBJECT);
	$return.= '<div id="keys">';
	// $return.= '<h2>'.$LANG->CHIFFRE_TITRE.'</h2>';
	$return.= '<ul>';
	foreach($chiffres as $chiffre){
		$return .= '<li>';
		if($chiffre->url != ''){
			$return.= '<a href="'.explode(';',$chiffre->url)[0].'" '.explode(';',$chiffre->url)[1].'">';
		}else{
			$return .= '<a href="#">';
		}
		$return.= '<div style="height:8rem"><img alt="" src="'.$chiffre->src.'"></div>';
		$return.= '<p>'.$chiffre->valeur.'<br>'.$chiffre->description.'</p>';
		$return.= '</a>';
		$return.= '</li>';
	}
	$return .= '</ul>';
	$return .= '</div>';

	return $return;
}

function chiffre_get_term_lang(){
    $lang = get_bloginfo("language");
    $domaine = get_bloginfo("wpurl");
    $file = "fr.json" ;
    $rep  = "lang" ;
    switch ($lang) {
        case "fr-FR": $file = "fr.json" ;
            break;
        case "en-GB": $file = "en.json" ;
			break;
		case "es-ES": $file = "es.json";
			break;
    }
    $content = wp_remote_get($domaine.'/wp-content/plugins/plugin_chiffres/lang/'.$file);
    return json_decode($content["body"]);
}
