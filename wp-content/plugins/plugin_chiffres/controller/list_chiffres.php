<?php
    if(!isset($_GET['action'])){
        show_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_chiffres.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_chiffre($_GET['chiffre']);
        show_table();
    }

    function show_table(){
		$chiffres = get_chiffres();
		if(count($chiffres) > 0){
?>
			<div class="bootstrap-iso">
			<h1>Liste des chiffres clés</h1>
			<table class="table table-hover">
				<tr>
					<th>Valeur</th><th>Description</th><th>Ordre d\'affichage</th><th>Page</th><th>Actions</th>
				</tr>
<?php
			foreach($chiffres as $chiffre){
				echo '<tr>';
				echo '<td>'.$chiffre->valeur.'</td>';
				echo '<td>'.$chiffre->description.'</td>';
				echo '<td>'.$chiffre->ordre.'</td>';
				if($chiffre->url != ''){
					echo '<td>'.explode(';',$chiffre->url)[1].'</td>';
				}else{
					echo '<td></td>';
				}
				

				$delete = sprintf('<a class="chiffres_delete" value="'.$chiffre->description.'" href="?page=%s&action=%s&chiffre=%s">Corbeille</a>',$_REQUEST['page'],'delete',$chiffre->id_chiffre);
				$edit = sprintf('<a href="?page=%s&action=%s&chiffre=%s">Modifier</a>',$_REQUEST['page'],'edit',$chiffre->id_chiffre);

				echo '<td>'. $edit . ' | ' . $delete . '</td>';
				echo '</tr>';
			}
			echo '</table>';
			echo '</div>';
		}else{
			echo '<div class="wrap isite-chiffres">';
			echo '<h1>Liste des chiffres clés</h1>';
			echo '<p>Aucun chiffres clés disponible.</p>';
			echo '</div>';
		}
    }

    function get_chiffres(){
        global $wpdb;
        $table = $wpdb->prefix . "chiffres";
        $sql = "select * from " . $table;
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_chiffre($chiffre){
        global $wpdb;
        $table = $wpdb->prefix . "chiffres";
        $sql = "DELETE FROM " . $table. " WHERE id_chiffre = ".$chiffre;
        $wpdb->query($sql);
    }