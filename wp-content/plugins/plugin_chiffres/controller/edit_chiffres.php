<?php
	$msg = '';
	if(isset($_POST['add_chiffres'])){
		global $wpdb;
        $table = $wpdb->prefix . "chiffres";
        $sql = "UPDATE ". $table. " set description_en='".$_POST['chiffre_desc_en']."', description_es='".$_POST['chiffre_desc_es']."', url_en='".$_POST['chiffre_url_en']."', url_es='".$_POST['chiffre_url_es']."', valeur = '".$_POST['chiffre_valeur']."', description='".$_POST['chiffre_desc']."', ordre=".$_POST['chiffre_ordre']." , src='".$_POST['chiffre_src']."', url='".$_POST['chiffre_url']."'  WHERE id_chiffre =" . $_GET['chiffre'];
        if($wpdb->query($sql));
        {
            $msg = '<div class="alert alert-success" role="alert">
                    <strong>Succès, </strong> Modification enregistrée avec succès !
                </div>';
        }
	}

	if(isset($_GET['chiffre']) && $_GET['chiffre'] != ''){
        global $wpdb;
        $table = $wpdb->prefix . "chiffres";
        $sql = "SELECT * FROM ". $table. " WHERE id_chiffre = " . $_GET['chiffre'];
        $chiffre = $wpdb->get_results($sql, OBJECT);
        if(count($chiffre) >= 1){
            $chiffre = $chiffre[0];
            // var_dump($chiffre);

        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Vous essayez de modifier du contenu qui n\'existe pas !
                </div>';
			die();
        }
    }else{
        echo '<div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Vous essayez de modifier du contenu qui n\'existe pas !
                </div>';
		die();
    }

?>

<div class="bootstrap-iso">
	<h1>Édition chiffre clé</h1>
	<?php echo $msg ; ?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="chiffre_valeur">Valeur: </label>
            <input class="form-control" type="text" name="chiffre_valeur" id="chiffre_valeur" placeholder="exemple: 18%"  value="<?php echo $chiffre->valeur; ?>" />
        </div>
        <div class="form-group">
            <label for="chiffre_desc">Description française: </label>
            <input class="form-control" type="text" name="chiffre_desc" id="" placeholder="Description" value="<?php echo $chiffre->description; ?>"/>
        </div>
        <div class="form-group">
        	<label for="chiffre_desc_en">Description anglaise: </label>
			<input class="form-control" id="chiffre_desc_en" type="text" name="chiffre_desc_en" id="" placeholder="Description anglaise" value="<?php echo $chiffre->description_en; ?>"/>
		</div>
        <div class="form-group">
        	<label for="chiffre_desc_en">Description espagnole: </label>
			<input class="form-control" id="chiffre_desc_es" type="text" name="chiffre_desc_es" id="" placeholder="Description espagnole" value="<?php echo $chiffre->description_es; ?>"/>
		</div>
        <div class="form-group">
            <label for="chiffre_ordre">Ordre: </label>
            <input class="form-control" type="number" name="chiffre_ordre" value="<?php echo $chiffre->ordre; ?>" placeholder="1"/>
        </div>
        <div class="form-group">
            <label for="chiffre_url">Page française:</label>
            <input type="text" name="chiffre_url" id="chiffre_url" class="form-control" value="<?php echo $chiffre->url ; ?>">
        </div>
        <div class="form-group">
            <label for="chiffre_url_en">Page anglaise:</label>
            <input type="text" name="chiffre_url_en" id="chiffre_url_en" class="form-control" value="<?php echo $chiffre->url_en ; ?>">
        </div>
        <div class="form-group">
            <label for="chiffre_url_en">Page espagnole:</label>
            <input type="text" name="chiffre_url_es" id="chiffre_url_es" class="form-control" value="<?php echo $chiffre->url_es ; ?>">
        </div>
		<input type="hidden" name="chiffre_src" id="chiffre_src" value="<?php echo $chiffre->src ;?>"/>
        <img id="chiffre_img" style="border: 1px solid #000; width: 500px; height: 200px;" src="<?php echo $chiffre->src ;?>"/>
		<input type="button" value="Ajouter un picto" name="add_chiffre_picto" id="add_chiffre_picto" class="button button-primary" />
        <input type="submit" name="add_chiffres" id="add_chiffre" value="Modifier le chiffre clé" class="button button-primary" />
    </form>
</div>
