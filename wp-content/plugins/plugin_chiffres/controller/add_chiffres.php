<?php

	if(isset($_POST['add_chiffre'])){
		if(!empty($_POST['chiffre_valeur']) && !empty($_POST['chiffre_desc']) && !empty($_POST['chiffre_desc_en']) && !empty($_POST['chiffre_ordre']) && !empty($_POST['chiffre_src'])){
			global $wpdb;
			$table = $wpdb->prefix . "chiffres";
			$sql = "INSERT INTO ". $table ."(valeur, description, description_en, description_es, ordre, src, url, url_en, url_es) VALUES('".htmlspecialchars($_POST['chiffre_valeur'],ENT_QUOTES)."','".htmlspecialchars($_POST['chiffre_desc'],ENT_QUOTES)."','".htmlspecialchars($_POST['chiffre_desc_en'],ENT_QUOTES)."', '".htmlspecialchars($_POST['chiffre_desc_es'], ENT_QUOTES)."', ".$_POST['chiffre_ordre'].",'".$_POST['chiffre_src']."', '".$_POST['chiffre_url']."', '".$_POST['chiffre_url_en']."', '".$_POST['chiffre_url_es']."' )";
			$wpdb->query($sql);
			$msg = '<div class="alert alert-success" role="alert">
                    <strong>Succès !</strong> Chiffre clé enregistré avec succès !
                </div>';
		}else{
			$msg = '<div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Veuillez remplir tous les champs !
                </div>';
		}
	}
	else{
		
	}
?>

<div class="bootstrap-iso">
	<h1>Ajouter un chiffre clé</h1>
	<?php echo $msg ; ?>
    <form action="" method="POST">
		<div class="form-group">
			<label for="chiffre_valeur">Valeur: </label>
			<input class="form-control" id="chiffre_valeur" type="text" name="chiffre_valeur" id="chiffre_valeur" placeholder="exemple: 18%"  value="<?php echo $_POST['chiffre_valeur']; ?>" />
		</div>
		<div class="form-group">
        	<label for="chiffre_desc">Description française: </label>
			<input class="form-control" id="chiffre_desc" type="text" name="chiffre_desc" id="" placeholder="Description" value="<?php echo $_POST['chiffre_desc']; ?>"/>
		</div>
		<div class="form-group">
        	<label for="chiffre_desc_en">Description anglaise: </label>
			<input class="form-control" id="chiffre_desc_en" type="text" name="chiffre_desc_en" id="" placeholder="Description anglaise" value="<?php echo $_POST['chiffre_desc_en']; ?>"/>
		</div>
		<div class="form-group">
        	<label for="chiffre_desc_en">Description espagnole: </label>
			<input class="form-control" id="chiffre_desc_en" type="text" name="chiffre_desc_es" id="" placeholder="Description espagnole" value="<?php echo $_POST['chiffre_desc_es']; ?>"/>
		</div>
		<div class="form-group">
			<label for="chiffre_ordre">Ordre: </label>
			<input class="form-control" id="chiffre_ordre" type="number" name="chiffre_ordre" value="<?php echo $_POST['chiffre_ordre']; ?>" placeholder="1"/>
		</div>
		<div class="form-group">
            <label for="chiffre_url">Page française:</label>
            <input type="text" name="chiffre_url" id="chiffre_url" class="form-control" value="<?php echo $_POST['chiffre_url']; ?>">
        </div>
        <div class="form-group">
            <label for="chiffre_url_en">Page anglaise:</label>
            <input type="text" name="chiffre_url" id="chiffre_url_en" class="form-control" value="<?php echo $_POST['chiffre_url_en']; ?>">
        </div>
        <div class="form-group">
            <label for="chiffre_url_en">Page espagnole:</label>
            <input type="text" name="chiffre_url" id="chiffre_url_es" class="form-control" value="<?php echo $_POST['chiffre_url_es']; ?>">
        </div>

		<img id="chiffre_img" style="border: 1px solid #000; width: 500px; height: 200px;"/>
		<input type="hidden" name="chiffre_src" id="chiffre_src" value="<?php echo $_POST['chiffre_src']; ?>" />
		<input type="button" value="Ajouter un picto" name="add_chiffre_picto" id="add_chiffre_picto" class="btn btn-primary" />
		<input type="submit" name="add_chiffre" id="add_chiffre" value="Ajouter le chiffre clé" class="btn btn-primary" />
		
    </form>
</div>
