jQuery(document).ready(function($){

    var eles = $('.chiffres_delete');
    console.log('chiffres.js');
    eles.each(function(){
        $(this).click(function(event){
            console.log('test');
            if(confirm("Êtes vous sur de vouloir supprimer '" + $(this).attr('value')+ "' ?")){
            }else{
                event.preventDefault();
            }
        });
    });

    var add_chiffre_picto = document.getElementById("add_chiffre_picto");
    var img       = document.getElementById("chiffre_img");
    var hidden    = document.getElementById("chiffre_src");

    if(hidden != null && hidden.value != ''){ img.src = hidden.value ; }
    if(img != null && img.src != '') { img.style.height= 'auto'; }


    var customUploader_chiffre = wp.media({ title: 'Sélectionner une image', button: { text: 'Utiliser cette Image'}, multiple: false});

    if(add_chiffre_picto != null){add_chiffre_picto.addEventListener( 'click', function() {
            console.log("test");
            if(customUploader_chiffre){ customUploader_chiffre.open(); }
        });
    }

    customUploader_chiffre.on( 'select', function(){
        var attachment = customUploader_chiffre.state().get('selection').first().toJSON();
        hidden.setAttribute('value', attachment.url);
        img.setAttribute('src',attachment.url);
        img.style.height= 'auto';
    });

});


// 