jQuery(document).ready(function($){

    var eles = $('.partenaires_delete');
    console.log('partenaires.js');
    eles.each(function(){
        $(this).click(function(event){
            console.log('test');
            if(confirm("Êtes vous sur de vouloir supprimer '" + $(this).attr('value')+ "' ?")){
            }else{
                event.preventDefault();
            }
        });
    });

    var addButton = document.getElementById("add_partenaires_img");
    var img       = document.getElementById("par_img");
    var hidden    = document.getElementById("par_src");
    if(hidden != null && hidden.value != ''){ img.src = hidden.value ; }
    if(img != null && img.src != '') { img.style.height= 'auto'; }


    var customUploader = wp.media({ title: 'Sélectionner une image', button: { text: 'Utiliser cette Image'}, multiple: false});

    if(addButton != null){addButton.addEventListener( 'click', function() {
            if(customUploader){ customUploader.open(); }
        });
    }

    customUploader.on( 'select', function(){
        var attachment = customUploader.state().get('selection').first().toJSON();
        hidden.setAttribute('value', attachment.url);
        img.setAttribute('src',attachment.url);
        img.style.height= 'auto';
    });

});


// 