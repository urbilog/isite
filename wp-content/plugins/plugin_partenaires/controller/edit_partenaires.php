<?php
    function show_cat($id_cat){
        global $wpdb;
        $table = $wpdb->prefix . "cat_partenaires";
        $sql = "SELECT * FROM " . $table;
        $categories = $wpdb->get_results($sql, OBJECT);
        foreach($categories as $cat){
            if($id_cat == $cat->id_cat_partenaires){
                echo '<option  selected="selected" value="'.$cat->id_cat_partenaires.'">'.$cat->libelle.'</option>';
            }else{
                echo '<option value="'.$cat->id_cat_partenaires.'">'.$cat->libelle.'</option>';
            }
        }
    }

    function update_par($nom, $lien, $cat, $ordre, $src){
        global $wpdb;
        $table = $wpdb->prefix . "partenaires";
        $sql = "UPDATE " . $table . " SET nom='".$nom."', lien='".$lien."', id_cat=".$cat.", ordre=".$ordre.", src='".$src."' WHERE id_partenaires = " . $_GET['par'];
        $wpdb->query($sql);

    }
?>
<div class="bootstrap-iso">
    <h1>Édition d'un partenaire</h1>
    <?php 
        if(isset($_POST['add_partenaires'])){
            if($_POST['par_nom'] != '' && $_POST['par_lien'] != '' && $_POST['par_cat'] != '' && $_POST['par_ordre'] != '' && $_POST['par_src'] != ''){
                echo '<div class="alert alert-success" role="alert">
                    <strong>Succès !</strong> Modification enregistrée avec succès !
                </div>';
                update_par($_POST['par_nom'], $_POST['par_lien'], $_POST['par_cat'], $_POST['par_ordre'], $_POST['par_src']);
            }else{
                echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Veillez à bien remplir tous les champs et ne pas oublier l\'image.
                </div>';
            }
        }

        if(isset($_GET['par']) && $_GET['par'] != ''){
            global $wpdb;
            $table = $wpdb->prefix . "partenaires";
            $sql = "SELECT * FROM ". $table. " WHERE id_partenaires = " . $_GET['par'];
            $par = $wpdb->get_results($sql, OBJECT);
            if(count($par) >= 1){ $par = $par[0]; }else{
                echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Vous essayez de modifier du contenu qui n\'existe pas !.
                </div>';
                die();
            }
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Vous essayez de modifier du contenu qui n\'existe pas !.
                </div>';
        }
?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="par_nom">Nom: </label>
            <input type="text" name="par_nom" id="par_nom" placeholder="Nom du partenaire" class="form-control" value="<?php echo $par->nom; ?>" />
        </div>
        <div class="form-group">
            <label for="par_lien">Lien: </label>
            <input type="text" name="par_lien" id="par_lien" placeholder="http://www.site.fr" class="form-control" value="<?php echo $par->lien; ?>"/>
        </div>
        <div class="form-group">
            <label for="par_cat">Catégorie</label>
            <select name="par_cat" class="form-control" id="par_cat"> <?php show_cat($par->id_cat); ?> </select>
        </div>
        <div class="form-group">
            <label for="par_ordre">Ordre d'affichage:</label>
            <input type="number" class="form-control" name="par_ordre" id="par_ordre" placeholder="1" max="50" min="1" value="<?php echo $par->ordre; ?>"/>
        </div>
        <img  id="par_img" src="<?php echo $par->src ; ?>" style="border: 1px solid #000; width: 500px; height: 200px;"/>
        
        <input type="button" name="add_partenaires_img" value="Modifier l'image" id="add_partenaires_img" class="btn btn-primary" />
        <input type="hidden" name="par_src" id = "par_src" value="<?php echo $par->src ; ?>"/>
        <input type="submit" name="add_partenaires" id="add_partenaires" value="Modifier le partenaire" class="btn btn-primary" />
    </form>
</div>


