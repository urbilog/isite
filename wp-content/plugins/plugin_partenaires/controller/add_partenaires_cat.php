<div class="bootstrap-iso">
<?php

    if(isset($_POST['add_partenaires_cat'])){
        global $wpdb;
	    $table = $wpdb->prefix . "cat_partenaires";
        $sql = "INSERT INTO ". $table ."(libelle, libelle_en, description, description_en, ordre) VALUES('"
        .htmlspecialchars($_POST['cat_libelle'],ENT_QUOTES)."','"
        .htmlspecialchars($_POST['cat_libelle_en'],ENT_QUOTES)."','"
        .htmlspecialchars($_POST['cat_desc'],ENT_QUOTES)."','"
        .htmlspecialchars($_POST['cat_desc_en'],ENT_QUOTES)
        ."', ".$_POST['cat_ordre'].")";


        $wpdb->query($sql);
        if($_POST['cat_libelle'] != '' && $_POST['cat_ordre'] != ''){
            echo '<div class="alert alert-success" role="alert">
                    <strong>Succès !</strong> Catégorie enregistrée avec succès !
                </div>';
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Veillez à bien remplir tous les champs !
                </div>';
        }
    }
?>
    <h1>Ajouter une catégorie de partenaire</h1>
    <form action="" method="POST">
        <div class="form-group">
            <label for="cat_libelle">Libellé français:</label>
            <input type="text" class="form-control" name="cat_libelle" id="cat_libelle" placeholder="ex: Collectivité" />
        </div>
        <div class="form-group">
            <label for="cat_libelle_en">Libellé anglais:</label>
            <input type="text" class="form-control" name="cat_libelle_en" id="cat_libelle_en" placeholder="ex: Collectivity" />
        </div>
        <div class="form-group">
            <label for="cat_desc">Description française:</label>
            <textarea name="cat_desc" class="form-control" id="cat_desc" placeholder="ex: Votre texte"></textarea>
        </div>
        <div class="form-group">
            <label for="cat_desc_en">Description anglaise:</label>
            <textarea name="cat_desc_en" class="form-control" id="cat_desc_en" placeholder="ex: Votre texte"></textarea>
        </div>
        <div class="form-group">
            <label for="cat_ordre">Ordre d'affichage:</label>
            <input type="number" class="form-control" name="cat_ordre" id="cat_ordre" placeholder="ex: 1" max="10" min="1"/>
        </div>
        <input type="submit" name="add_partenaires_cat" value="Ajouter" class="btn btn-primary" />
    </form>
</div>