<?php
    $msg = '';
    if(isset($_POST['edit_partenaires_cat'])){
        global $wpdb;
        $table = $wpdb->prefix . "cat_partenaires";
        $sql = "UPDATE ". $table
        ." set libelle = '".$_POST['cat_libelle']
        ."', libelle_en = '".$_POST['cat_libelle_en']
        ."', description='".$_POST['cat_desc']
        ."', description_en='".$_POST['cat_desc_en']
        ."', description_es='".$_POST['cat_desc_es']
        ."', libelle_es='".$_POST['cat_libelle_es']

        ."', ordre=".$_POST['cat_ordre']
        ." WHERE id_cat_partenaires =" . $_GET['cat'];
        if($wpdb->query($sql))
        {
            $msg = '<div class="alert alert-success" role="alert">
                        <strong>Succès !</strong> Catégorie enregistrée avec succès !
                    </div>';
        }else{
            $msg = '<div class="alert alert-danger" role="alert">
                        <strong>Attention !</strong> Veillez à bien remplir tous les champs !
                    </div>';
        }
    }

    if(isset($_GET['cat']) && $_GET['cat'] != ''){
        global $wpdb;
        $table = $wpdb->prefix . "cat_partenaires";
        $sql = "SELECT * FROM ". $table. " WHERE id_cat_partenaires = " . $_GET['cat'];
        $cat = $wpdb->get_results($sql, OBJECT);
        if(count($cat) >= 1){
            $cat = $cat[0];
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Attention !</strong> Vous essayez de modifier un contenu qui n\'existe pas !
                </div>';
        }
    }else{
        echo '<div class="alert alert-danger" role="alert">
                <strong>Attention !</strong> Vous essayez de modifier un contenu qui n\'existe pas !
            </div>';

    }

?>
<div class="bootstrap-iso">
    <h1>Édition catégorie</h1>
    <?php echo $msg ; ?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="cat_libelle">Libellé français: </label>
            <input class="form-control" type="text" value="<?php echo $cat->libelle; ?>" name="cat_libelle" id="cat_libelle" placeholder="ex: Collectivité" />
        </div>
        <div class="form-group">
            <label for="cat_libelle_en">Libellé anglais: </label>
            <input class="form-control" type="text" value="<?php echo $cat->libelle_en; ?>" name="cat_libelle_en" id="cat_libelle_en" placeholder="ex: Collectivité" />
        </div>
        <div class="form-group">
            <label for="cat_libelle_en">Libellé espagnole: </label>
            <input class="form-control" type="text" value="<?php echo $cat->libelle_es; ?>" name="cat_libelle_es" id="cat_libelle_es" placeholder="ex: Collectivité" />
        </div>
        <div class="form-group">
            <label for="cat_desc">Description française: </label>
            <textarea class="form-control" name="cat_desc" id="cat_desc" placeholder="ex: Votre texte"><?php echo $cat->description; ?></textarea>
        </div>
        <div class="form-group">
            <label for="cat_desc_en">Description anglaise: </label>
            <textarea class="form-control" name="cat_desc_en" id="cat_desc_en" placeholder="ex: Votre texte"><?php echo $cat->description_en; ?></textarea>
        </div>
        <div class="form-group">
            <label for="cat_desc_en">Description espagnole: </label>
            <textarea class="form-control" name="cat_desc_es" id="cat_desc_es" placeholder="ex: Votre texte"><?php echo $cat->description_es; ?></textarea>
        </div>
        <div class="form-group">
            <label for="cat_ordre">Ordre d'affichage:</label>
            <input class="form-control" type="number" name="cat_ordre" id="cat_ordre" value="<?php echo $cat->ordre; ?>"/>
        </div>
        <input type="submit" name="edit_partenaires_cat" value="Modifier" class="btn btn-primary" />
    </form>
</div>



