<?php
    if(!isset($_GET['action'])){
        show_cat_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_partenaires_cat.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_par_cat($_GET['cat']);
        show_cat_table();
    }

    function show_cat_table(){
        $categories = get_cat_partenaires();
        if(count($categories)){
?>
            <div class="bootstrap-iso">
                <h1>Liste des catégories de partenaires</h1>
                <table class="table table-hover">
                    <tr><th>Libellé</th><th>Description</th><th>Ordre d\'affichage</th><th>Actions</th></tr>
<?php
            foreach($categories as $cat){
                echo '<tr>';
                echo '<td>'.$cat->libelle.'</td>';
                echo '<td>'.$cat->description.'</td>';
                echo '<td>'.$cat->ordre.'</td>';

                $delete = sprintf('<a class="partenaires_delete" value="'.$cat->libelle.'" href="?page=%s&action=%s&cat=%s">Corbeille</a>',$_REQUEST['page'],'delete',$cat->id_cat_partenaires);
                $edit = sprintf('<a href="?page=%s&action=%s&cat=%s">Modifier</a>',$_REQUEST['page'],'edit',$cat->id_cat_partenaires);

                echo '<td>'. $edit . ' ' . $delete . '</td>';
                echo '</tr>';
            }
?>
                </table>
            </div>
<?php
        }else{
?>
            <div class="wrap">
                <h1>Liste des catégories de partenaires</h1>
                <p>Aucune catégorie de partenaires disponible.</p>
            </div>';
<?php
        }
        
    }

    function get_cat_partenaires(){
        global $wpdb;
        $table = $wpdb->prefix . "cat_partenaires";
        $sql = "select * from ".$table ;
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_par_cat($cat){
        global $wpdb;
        $table = $wpdb->prefix . "cat_partenaires";
        $sql = "DELETE FROM " . $table. " WHERE id_cat_partenaires = ".$cat;
        $wpdb->query($sql);
    }
    
?>

