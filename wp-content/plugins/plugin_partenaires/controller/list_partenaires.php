<?php
    if(!isset($_GET['action'])){
        show_table();
    }else if(isset($_GET['action']) && $_GET['action']=='edit'){
        include ('edit_partenaires.php');
    }else if(isset($_GET['action']) && $_GET['action']=='delete'){
        delete_par($_GET['par']);
        show_table();
    }

    function show_table(){
            $partenaires = get_partenaires();
            if(count($partenaires) > 0){
    ?>
                <div class="bootstrap-iso">
            <h1>Liste des catégories de partenaires</h1>
            <a href="?page=plugin_partenaires_add">Ajouter un partenaire</a>
            <table class="table table-hover">
                <tr>
                    <th>Nom</th><th>Catégorie</th><th>Ordre d\'affichage</th><th>Lien</th><th>Actions</th>
                </tr>
    <?php
            foreach($partenaires as $par){
    
                echo '<tr>';
                echo '<td>'.$par->nom.'</td>';
                echo '<td>'.$par->libelle.'</td>';
                echo '<td>'.$par->ordre.'</td>';
                echo '<td>'.$par->lien.'</td>';

                $delete = sprintf('<a class="partenaires_delete" value="'.$par->nom.'" href="?page=%s&action=%s&par=%s">Corbeille</a>',$_REQUEST['page'],'delete',$par->id_partenaires);
                $edit = sprintf('<a href="?page=%s&action=%s&par=%s">Modifier</a>',$_REQUEST['page'],'edit',$par->id_partenaires);

                echo '<td>'. $edit . ' | ' . $delete . '</td>';
                echo '</tr>';
            }
    ?>
                </table>
            </div>
    <?php
        }else{ ?>
            <div class="bootstrap-iso">';
                <h1>Liste des catégories de partenaires</h1>
                <p>Aucun partenaire disponible.</p>
            </div>
    <?php
        }
    }

    function get_partenaires(){
        global $wpdb;
        $table2 = $wpdb->prefix . "cat_partenaires";
        $table1 = $wpdb->prefix . "partenaires";
        $sql = "select id_cat, id_partenaires, nom, src, p.ordre as ordre, lien, libelle from ".$table1." p INNER JOIN ".$table2." c ON c.id_cat_partenaires = p.id_cat" ;
        return $wpdb->get_results($sql, OBJECT);
    }

    function delete_par($par){
        global $wpdb;
        $table = $wpdb->prefix . "partenaires";
        $sql = "DELETE FROM " . $table. " WHERE id_partenaires = ".$par;
        $wpdb->query($sql);
    }