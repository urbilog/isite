<?php
    function show_cat($select){
        global $wpdb;
        $table = $wpdb->prefix . "cat_partenaires";
        $sql = "SELECT * FROM " . $table;
        $categories = $wpdb->get_results($sql, OBJECT);
        foreach($categories as $cat){

            if($select == $cat->id_cat_partenaires){
                echo '<option selected="selected" value="'.$cat->id_cat_partenaires.'">'.$cat->libelle.'</option>';
            }else{
                echo '<option value="'.$cat->id_cat_partenaires.'">'.$cat->libelle.'</option>';
            }
        }
    }

    function submit(){
        if(isset($_POST['add_partenaires']) && $_POST['par_nom'] != '' && $_POST['par_lien'] != '' && $_POST['par_cat'] != '' && $_POST['par_ordre'] != '' && $_POST['par_src'] != ''){
            echo '<div class="alert alert-success" role="alert">
                    <strong>Succès !</strong> Modification enregistrée avec succès !
                </div>';
            insert_par($_POST['par_nom'], $_POST['par_lien'], $_POST['par_cat'], $_POST['par_ordre'], $_POST['par_src']);
        }else{
            echo '<div class="alert alert-danger" role="alert">
                    <strong>Erreur !</strong> Veillez à bien remplir tous les champs et ne pas oublier l\'image.
                </div>';
        }
    }

    function insert_par($nom, $lien, $cat, $ordre, $src){
        global $wpdb;
        $table = $wpdb->prefix . "partenaires";
        $sql = "INSERT INTO " . $table . "(nom, lien, id_cat, ordre, src) VALUES('".$nom."','".$lien."',".$cat.",".$ordre.",'".$src."')";
        $wpdb->query($sql);

    }
?>
<div class="bootstrap-iso">
    <h1>Ajouter un partenaire</h1>
    <?php if(isset($_POST['add_partenaires'])){submit(); }?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="par_nom">Nom: </label>
            <input type="text" id="par_nom" name="par_nom" id="par_nom" placeholder="Nom du partenaire"  class="form-control" value="<?php echo $_POST['par_nom']; ?>" />
        </div>
        <div class="form-group">
            <label for="par_lien">Lien: </label>
            <input type="text" id="par_lien" name="par_lien" class="form-control" id="" placeholder="http://www.site.fr" value="<?php echo $_POST['par_lien']; ?>"/>
        </div>
        <div class="form-group">
            <label for="par_cat">Catégorie</label>
            <select name="par_cat" id="par_cat" class="form-control" value="<?php echo $_POST['par_cat']; ?>">
                <?php show_cat($_POST['par_cat']); ?>
            </select>
        </div>
        <div class="form-group">
            <label for="par_ordre">Ordre d'affichage:</label>
            <input type="number" class="form-control" name="par_ordre" id="par_ordre" placeholder="1" max="50" min="1" value="<?php echo $_POST['par_ordre']; ?>"/>
        </div>
        <img  id="par_img" style="border: 1px solid #000; width: 500px; height: 200px;"/>
        <input type="hidden" name="par_src" id = "par_src" value="<?php echo $_POST['par_src']; ?>"/>
        <input type="button" name="add_partenaires_img" value="Ajouter une image" id="add_partenaires_img" class="btn btn-primary" />
        <input type="submit" name="add_partenaires" id="add_partenaires" value="Ajouter le partenaire" class="btn btn-primary" />

    </form>
</div>


