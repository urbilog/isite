<?php
/*
Plugin Name: plugin-partenaires
Description: Gestion des partenaires
Version: 0.1
Author: Urbilog
*/

register_activation_hook(__FILE__,'database_install'); 

register_deactivation_hook( __FILE__, 'database_delete' );

add_action('admin_menu', 'add_menu_plugin');

function database_install() {

	global $wpdb;

	$table1 = $wpdb->prefix . "cat_partenaires";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table1 . " (id_cat_partenaires int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, libelle TEXT, libelle_en TEXT, libelle_es TEXT, description TEXT, description_en TEXT, description_es TEXT,ordre int(10)) ENGINE=InnoDB";
	$wpdb->query($sql);

	$table2 = $wpdb->prefix . "partenaires";
	$sql = "CREATE TABLE IF NOT EXISTS " . $table2 . " (id_partenaires int(10) NOT NULL AUTO_INCREMENT PRIMARY KEY, id_cat int(10), nom TEXT ,src TEXT, ordre int(10), lien TEXT, CONSTRAINT fk_cat_partenaires FOREIGN KEY (id_cat) REFERENCES  ".$table1."(id_cat_partenaires) ON DELETE CASCADE) ENGINE=InnoDB;";
	$wpdb->query($sql);

	require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
}

add_action( 'admin_enqueue_scripts', 'plugin_partenaires_style' );

function plugin_partenaires_style() {
	wp_enqueue_style( 'carrousel_bootstrap', plugin_dir_url(__FILE__).'bootstrap.urbilog.css');
	wp_enqueue_style( 'partenaires_style', plugin_dir_url(__FILE__).'style.css' );
	wp_enqueue_script( 'partenaires_script', plugin_dir_url( __FILE__ ) . 'partenaires.js', array(), '1.0' );

}

function database_delete(){
	
}

function add_menu_plugin(){
	add_menu_page( 'Partenaires', 'Partenaires', '', 'plugin_partenaires_index', 'add_plugin_partenaires_index','dashicons-groups' );
	add_submenu_page( 'plugin_partenaires_index', 'Toutes les catégories', 'Toutes les catégories', 'read', 'plugin_partenaires_cat_list', 'add_plugin_partenaires_cat_list');
	add_submenu_page( 'plugin_partenaires_index', 'Ajouter catégorie', 'Ajouter catégorie', 'read', 'plugin_partenaires_cat_add', 'add_plugin_partenaires_cat_add');
	add_submenu_page( 'plugin_partenaires_index', 'Tous les partenaires', 'Tous les partenaires', 'read', 'plugin_partenaires_list', 'add_plugin_partenaires_list');
	add_submenu_page( 'plugin_partenaires_index', 'Ajouter partenaire', 'Ajouter partenaire', 'read', 'plugin_partenaires_add', 'add_plugin_partenaires_add');
}

function add_plugin_partenaires_index() {
    include('controller/index.php');
}
function add_plugin_partenaires_add() {
    include('controller/add_partenaires.php');
}
function add_plugin_partenaires_cat_add() {
    include('controller/add_partenaires_cat.php');
}
function add_plugin_partenaires_cat_list(){
	include('controller/list_partenaires_cat.php');
}
function add_plugin_partenaires_list(){
	include('controller/list_partenaires.php');
}

add_shortcode('partenaires', 'show_partenaires');
function show_partenaires(){
	$lang = get_locale();
	global $wpdb;
	$table1 = $wpdb->prefix . "partenaires";
	$table2 = $wpdb->prefix . "cat_partenaires";
    $sql = "select * from ".$table1." p right join ". $table2 ." c on p.id_cat = c.id_cat_partenaires order by c.ordre, p.ordre";
	$partenaires = $wpdb->get_results($sql, OBJECT);
	$code = '';
	if(count($partenaires) >= 1){
		$category = $partenaires[0]->libelle;
		if($lang == 'en_GB'){
			$code .= '<h2>'.$partenaires[0]->libelle_en.'</h2>';
			$code .= '<p>'.$partenaires[0]->description_en .'</p>';
		}else if($lang == 'es_ES'){
			$code .= '<h2>'.$partenaires[0]->libelle_es.'</h2>';
			$code .= '<p>'.$partenaires[0]->description_es .'</p>';
		}else{
			$code .= '<h2>'.$partenaires[0]->libelle.'</h2>';
			$code .= '<p>'.$partenaires[0]->description .'</p>';
		}

		$code .= '<ul class="partenaires">';
		foreach($partenaires as $partenaire){
			if($category != $partenaire->libelle){
				$code .= '</ul>';
				if($lang == 'en_GB'){
					$code .= '<h2>'.$partenaire->libelle_en.'</h2>';
					$code .= '<p>'.$partenaire->description_en .'</p>';
				}else if($lang == 'es_ES'){
					$code .= '<h2>'.$partenaire->libelle_es.'</h2>';
					$code .= '<p>'.$partenaire->description_es .'</p>';
				}else{
					$code .= '<h2>'.$partenaire->libelle.'</h2>';
					$code .= '<p>'.$partenaire->description .'</p>';
				}
				if($partenaire->nom != ''){
					$code .= '<ul class="partenaires">';
					$code .= '<li>';
					$code .= '<a href="'.$partenaire->lien.'">';
					if($lang == 'en_GB'){
						$code .= '<img src="'.$partenaire->src.'" alt="'.$partenaire->nom.' website"/>';
					}else{
						$code .= '<img src="'.$partenaire->src.'" alt="vers le site '.$partenaire->nom.'"/>';
					}
					// $code .= '<img src="'.$partenaire->src.'" alt="'.$partenaire->nom.'"/>';
					$code .= '<div class="overlay"><h2>'.$partenaire->nom.'</h2></div>';
					$code .= '</a>';
					$code .= '</li>';
				}
			}else{
				$code .= '<li>';
				$code .= '<a href="'.$partenaire->lien.'">';
				if($lang == 'en_GB'){
					$code .= '<img src="'.$partenaire->src.'" alt="'.$partenaire->nom.' website"/>';
				}else{
					$code .= '<img src="'.$partenaire->src.'" alt="vers le site '.$partenaire->nom.'"/>';
				}
				$code .= '<div class="overlay"><h2>'.$partenaire->nom.'</h2></div>';
				$code .= '</a>';
				$code .= '</li>';
			}
			$category = $partenaire->libelle;
		}
	}
	
	return $code ;
}



