<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$LANG = get_term_lang();
get_header(); ?>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<article id="post-17" class="post-17 page type-page status-publisg hentry">
				<header class="entry-header">
					<h1 class="entry-title"><?php echo get_queried_object()->name; ?></h1>
				</header>
				<div class="entry-content">
					<div class="entry-content">
						<div id="main-top">
							<div id="top-wrap">
								<div id="main-panel">
								</div>
							</div>
						</div>
						<div id="filter" class="appels_a_projets"><ul>
						<ul class="">
						<?php
						function trie($x, $y){
							$x = new DateTime($x);
							$y = new DateTime($y);
							if($x > $y){
								return -1;
							}else if($y > $x){
								return 1;
							}else if($x == $y){
								return 0;
							}
						}
						$args = array('hide_empty' => 0, 'hierarchical' => true, 'title_li' => '', 'taxonomy' => 'app_category', 'current_category' => 1, 'show_option_none'  => '',
						);
							$filtre = wp_list_categories($args);
						?></ul></div>
						<div id="les_appels" class="les_appels">
						<?php
							$apps = array();
							if ( have_posts() ) :
								while ( have_posts() ) : the_post();
								$key = get_field('fin_app');
								if(isset($apps[$key])){
									array_push($apps[$key], $post);
								}else{
									$apps[$key] = [];
									array_push($apps[$key], $post);
								}
								
								endwhile;
							endif;

							uksort($apps, 'trie');

							if ( have_posts() ) :
								foreach($apps as $app_array){
									foreach($app_array as $app){
										$date = new DateTime(get_field('fin_app', $app->ID));
										$date_debut = new DateTime(get_field('date_de_debut_app', $app->ID));
										$current_date = new DateTime();
										$post_lang = pll_get_post_language($app->ID , 'slug');
		
										switch($post_lang){
											case "en" : $project_open = 'project_open_en';
																	$project_close = 'project_close_en';
												break;
											case "es" : $project_open = 'project_open_es';
																	$project_close = 'project_close_es';
												break;
											case "fr" : $project_open = 'project_open';
																	$project_close = 'project_close';
												break;
										}
		
										if($date_debut <= $current_date){
											if($current_date < $date){
												echo "<div class='project $project_open'>";
											}else{
												echo "<div class='project $project_close'>";
											}
		
										}
										echo '<h3>';
										echo get_the_title($app->ID);
										echo '</h3>';
										echo do_shortcode($app->post_content); ?>

										</div> <?php
									}
?>
							
							<?php
							}
							else :
								get_template_part( 'template-parts/post/content', 'none' );
							endif;
							?>
						</div>
						<!-- <div class="navigation"><p><?php// posts_nav_link(); ?></p></div> -->
						<div class="navigation_publi">
								<?php if(get_bloginfo("language") == 'en-GB'){ ?>
										<?php posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Previous calls for projects</p>','<p id="next">Next alls for projects <span aria-hidden="true">&gt;</span></p>'); ?>
								<?php }else if(get_bloginfo("language") == 'es'){ ?>
										<?php posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Convocatorias anteriores</p>','<p id="next">Convocatorias siguientes <span aria-hidden="true">&gt;</span></p>'); ?>
								<?php }else{ ?>
										<?php posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Les APP précédents</p>','<p id="next">Les APP suivants <span aria-hidden="true">&gt;</span></p>'); ?>
								<?php } ?>
						</div>
					</div>
				</div>
			</article>

		</main><!-- #main -->
</div><!-- .wrap -->

<?php get_footer();
