
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
include("wp-content/themes/isite/conf.inc.php");
switch($GLOBALS['COULEUR']){
  case 'orange' :
    $ns_soutenir = WP_SITEURL. '/wp-content/images/logo-nous-soutenir-orange.png';
    $ap_projet   = WP_SITEURL. '/wp-content/images/logo-appels-a-projet-orange.png';
  break;
  case 'orange-light' :
    $ns_soutenir = WP_SITEURL. '/wp-content/images/logo-nous-soutenir-orange.png';
    $ap_projet   = WP_SITEURL. '/wp-content/images/logo-appels-a-projet-orange.png';
  break;
  case 'blue' :
    $ns_soutenir = WP_SITEURL. '/wp-content/images/logo-nous-soutenir-blue.png';
    $ap_projet   = WP_SITEURL. '/wp-content/images/logo-appels-a-projet-blue.png';
  break;
}?>
 </div>
        <footer id="footer" class>
            <div class="header">
              <div class="left">
                  <?php //$LANG = get_term_lang(); ?>
                      <a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_NOUS_SOUTENIR; ?>" id="ns_soutenir">
                          <img alt="" src="<?php echo $ns_soutenir; ?>" />
                          <p><?php echo $GLOBALS['LANG']->ISITE_NOUS_SOUTENIR; ?></p>
                      </a>
                      <a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_APPELS_A_PROJET; ?>" id="ap_projet">
                          <img alt="" src="<?php echo $ap_projet; ?>" />
                          <p><?php echo $GLOBALS['LANG']->ISITE_APPELS_A_PROJET; ?></p>
                      </a>
                </div>
                <div class="right">
                    <div class="float-right">
                      <a href="https://twitter.com/isiteULNE">
                          <img class="network" alt="<?php echo $GLOBALS['LANG']->ISITE_ALT_TWITTER; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-twitter.png" />
                      </a>
                      <a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_MESSAGE; ?>">
                          <img class="network" alt="<?php echo $GLOBALS['LANG']->ISITE_ALT_MESSAGE; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-message.png" />
                      </a>
                      <a href="https://fr.linkedin.com/company/isite-ulne">
                          <img class="network" alt="<?php echo $GLOBALS['LANG']->ISITE_ALT_LINKEDIN; ?>" src="<?= WP_SITEURL ?>/wp-content/images/linkedin.png" />
                      </a>
                      <a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_ALERTES; ?>">
                          <img class="network" alt="<?php echo $GLOBALS['LANG']->ISITE_ALT_RSS; ?>" src="<?= WP_SITEURL ?>/wp-content/images/iconeRSS.png" />
                      </a>
                  </div>
                </div>
              </div>
            <?php
            wp_reset_query();
            if(is_front_page()){  ?>
                <ul class="partenaires-footer">
                    <li>
                        <a href="http://www.univ-lille.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/univ-lille.png" alt="Université de Lille website"/></a>
                    </li>
                    <li>
                        <a href="http://www.ec-lille.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/ec-lille.png" alt="Centrale Lille website"/></a>
                    </li>
                    <li>
                        <a href="http://www.ensc-lille.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/ensc-lille.png" alt="École de Chimie Lille website"/></a>
                    </li>
                    <li>
                        <a href="http://www.ensait.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/ensait.png" alt="ENSAIT website"/></a>
                    </li>

                    <li>
                        <a href="http://www.lille.archi.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/lille.archi.jpg" alt="ENSAP website"/></a>
                    </li>
                    <li>
                        <a href="http://www.sciencespo-lille.eu/"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/sciencespo-lille.png" alt="Sciences Po Lille website"/></a>
                    </li>
                    <li>
                        <a href="http://www.esj-lille.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/esj-lille.png" alt="esj-lille website"/></a>
                    </li>
                    <li>
                        <a href="http://www.imt-lille-douai.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/imt-lille-douai.png" alt="IMT Lille-Douai website"/></a>
                    </li>
                    <li>
                        <a href="http://www.artsetmetiers.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/artsetmetiers.jpg" alt="Arts et Métiers Paristech website"/></a>
                    </li>

                    <li>
                        <a href="http://www.cnrs.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/cnrs.png" alt="CNRS website"/></a>
                    </li>
                    <li>
                        <a href="http://www.inserm.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/inserm.png" alt="Inserm website"/></a>
                    </li>

                    <li>
                        <a href="http://www.inria.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/inria.png" alt="Inria website"/></a>
                    </li>
                    <li>
                        <a href="http://www.pasteur-lille.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/pasteur-lille.png" alt="Institut Pasteur de Lille website"/></a>
                    </li>
                    <li>
                        <a href="http://www.chru-lille.fr"><img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/chru-lille.png" alt="CHRU website"/></a>
                    </li>
                </ul>
            <?php } ?>

            <?php
            wp_reset_query(); ?>
            <img id="handitested" src="<?= WP_SITEURL ?>/wp-content/images/Handi_Tested-01.png" alt=''/>
            <?php
            if(!is_front_page()){ ?>
			    <a href="http://www.gouvernement.fr/secretariat-general-pour-l-investissement-sgpi"><img class="SIG" alt="SIG - investissements d'avenir" src="<?= WP_SITEURL ?>/wp-content/images/LOGO_Investirlavenir_RVB.png"/></a>
            <?php }?>

            <ul class="navList">
                <li><a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_MENTIONS_LEGALES; ?>"><?php echo $GLOBALS['LANG']->ISITE_MENTIONS_LEGALES; ?></a></li>
                <li><a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_PLAN_ACCES_ET_CONTACT; ?>"><?php echo $GLOBALS['LANG']->ISITE_PLAN_ACCES_ET_CONTACT;?></a></li>
                <li><a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_ACCESSIBILITE; ?>"><?php echo $GLOBALS['LANG']->ISITE_ACCESSIBILITE; ?></a></li>
                <?php if (get_bloginfo("language") != 'es'){ ?>
                  <li><a href="<?php echo $GLOBALS['LANG']->ISITE_LIEN_ESPACE_PRESSE; ?>"><?php echo $GLOBALS['LANG']->ISITE_ESPACE_PRESSE; ?></a></li>
                  <li><a href="/index.php/fr/interne/">Intranet</a></li>
                <?php } ?>

                <li><a id="contraste" href="#"><?php echo $GLOBALS['LANG']->ISITE_CONTRASTE_ELEVE; ?></a></li>
            </ul>
            Copyright © I-SITE Université Lille Nord-Europe <?php echo date('Y') ; ?>
    </footer>
    </div>
</div>
<?php wp_footer(); ?>
</body>
</html>
