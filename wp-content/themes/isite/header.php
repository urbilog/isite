<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
include("wp-content/themes/isite/header-content/head-header.php");
include("wp-content/themes/isite/conf.inc.php");

$GLOBALS["dir"]        = "/wp-content/images/";
$GLOBALS["ns_soutenir"] = $GLOBALS["dir"] . "logo-nous-soutenir-orange.png";
$GLOBALS["ap_projet"]   = $GLOBALS["dir"] . "logo-appels-a-projet-orange.png";
$GLOBALS["twitter"]     = $GLOBALS["dir"] . "logo-twitter-blanc.png";
$GLOBALS["msg"]         = $GLOBALS["dir"] . "logo-message-blanc.png";


$page = get_query_var('pagename');
$orange = array(
	'nous-soutenir',
	'plan-dacces-et-contact',
	'espace-presse',
	'contact',
	'la-fondation',
	'appel-a-projet',
	'le-consortium',
	'les-partenaires',
	'une-fondation-partenariale-pour-accompagner-le-projet-i-site-ulne',
	'gouvernance',
	'perimetre-dexcellence-de-li-site-peridex'
);

$orange_light = array(
	'nos-actions',
	'our-actions'
);

$blue = array(
	'mentions-legales',
	'accessibilite',
	'investir-pour-accompagner-la-creation-de-la-future-universite-lille-nord-europe',
	'structurer-la-recherche',
	'soutenir-linnovation-pedagogique-et-numerique',
	'accelerer-la-valorisation-le-transfert-de-la-recherche-et-la-collaboration-avec-les-entreprises',
	'augmenter-la-visibilite-internationale-et-creer-un-campus-europeen',
	'accompagny-the-creation-of-the-ulne',
	'structure-research',
	'support-pedagogical-and-digital-innovation',
	'strengthen-links-with-societies',
	'increase-international-visibility'
);

$category = get_the_category();
$cat = get_category( get_query_var( 'cat' ) );
?>
<body  <?php body_class($GLOBALS['COULEUR']); ?>>
<div id="page" class="site">
	<div class="site-inner">
	<?php include("wp-content/themes/isite/header-content/header-header.php"); ?>
	<ul id="breadcrumb">
		<?php
		wp_reset_query();
		if(!is_front_page() && function_exists('bcn_display')) { bcn_display(); } ?>
	</ul>
