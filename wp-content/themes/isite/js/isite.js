var currentCar = 0;
var touchstartX = 0;
var touchstartY = 0;
var touchendX = 0;
var touchendY = 0;


var recent_index = 3;

var ul_lang = document.querySelector('#lang');
var li_es = document.querySelector('.lang-item-es');
// ul_lang.removeChild(li_es);

var lang = document.querySelector('html').lang;
var contr = localStorage.getItem("isite-contrast");
if(contr)
{
  document.body.classList.add("contrast");
  if(lang == 'fr-FR'){
    document.querySelector('#contraste').innerHTML = "Contraste classique";
  }else if(lang == 'en-EN'){
    document.querySelector('#contraste').innerHTML = "Classic contrast";
  }else{
    document.querySelector('#contraste').innerHTML = "Contraste clásico";
  }
}
var contrast = document.querySelector("#contraste");
contrast.addEventListener('click', function(e){
    e.preventDefault();
    var contrast = localStorage.getItem("isite-contrast");
    if(contrast)
    {
        document.body.classList.remove("contrast");
        localStorage.removeItem("isite-contrast");
        if(lang == 'fr-FR'){
            document.querySelector('#contraste').innerHTML = "Contraste élevé";
        }else if(lang == 'en-EN'){
            document.querySelector('#contraste').innerHTML = "High contrast";
        }else{
            document.querySelector('#contraste').innerHTML = "Gran contraste";
        }
    }else{
        document.body.classList.add("contrast");
        localStorage.setItem("isite-contrast", "true");
        if(lang == 'fr-FR'){
            document.querySelector('#contraste').innerHTML = "Contraste classique";
        }else{
            document.querySelector('#contraste').innerHTML = "Classic contrast";
        }
    }
});



setInterval(windowsize, 2000);

function windowsize(){
    if(window.innerWidth > 992){
        eles = document.querySelectorAll("menu-principal > .opened");
        eles = document.getElementsByClassName("opened");
        if(eles.length > 0){
            for (i = 0; i < eles.length; i++) {
               removeClass(eles[i], 'opened');
            }
        }
    }
}

function hasClass(ele,cls) {
  if(ele){
    return !!ele.className.match(new RegExp('(\\s|^)'+cls+'(\\s|$)'));
  }
}
function addClass(ele,cls) {
  if (!hasClass(ele,cls)) ele.className += " "+cls;
}
function removeClass(ele,cls) {
  if (hasClass(ele,cls)) {
    var reg = new RegExp('(\\s|^)'+cls+'(\\s|$)');
    ele.className=ele.className.replace(reg,' ');
  }
}

document.addEventListener('DOMContentLoaded', function(){
    objectFitImages();
    var searchLabel = document.querySelector('.searchLabel');
    var search = document.querySelector('#search');
search.addEventListener('focus', function(){
    console.log('onfocus');
    console.log(this);
    searchFocusIn();
});

search.addEventListener('onfocusout', function(){
    console.log('onfocusout');
    searchFocusOut();
});

function searchFocusIn(){
    document.getElementById("submitSearch").style.zIndex=5;
}
function searchFocusOut(){
    setTimeout(function(){
        document.getElementById("submitSearch").style.zIndex=0;
    },1000);
}
    eles = document.querySelectorAll("#menu-nav-mobile  li  a");
    for(var i in eles){
        if(eles[i].nextSibling != undefined){
                eles[i].innerHTML = '<span class="link">' + eles[i].innerHTML + '</span>';
                eles[i].innerHTML = '<a href="#" role="button" class="arrow down" aria-expanded="false"></a>' + eles[i].innerHTML;
            }else{
                eles[i].innerHTML = '<span class="blank"></span>' + eles[i].innerHTML;
                eles[i].innerHTML = '<span class="link">' + eles[i].innerHTML + '</span>';
            }
    }

    var arrow = document.querySelectorAll('a.arrow');
        for(var i =  0; i < arrow.length ; i++){
            arrow[i].addEventListener('click', function(e){
                e.preventDefault();
                var ul = this.parentNode.nextElementSibling ;
                if(this.classList.contains('down')){
                    this.classList.remove('down');
                    this.classList.add('right');
                    this.setAttribute('aria-expanded', 'true');
                    ul.style.display = 'block';
                }else{
                    this.classList.remove('right');
                    this.classList.add('down');
                    ul.style.display = 'none';
                    this.setAttribute('aria-expanded', 'false');
                }
            });
        }

});

function burgerMenu(e){
    e.preventDefault();
    if(hasClass(document.querySelector('#menu-nav-mobile'),"menuOpen")){
        removeClass(document.querySelector('#menu-nav-mobile'),"menuOpen");
        document.querySelector('#menu-button').setAttribute('aria-expanded', 'false');
        document.querySelector('#menu-button > img').setAttribute('alt', 'Ouvrir le menu');
    }else{
        addClass(document.querySelector('#menu-nav-mobile'),"menuOpen");
        document.querySelector('#menu-button').setAttribute('aria-expanded', 'true');
        document.querySelector('#menu-button > img').setAttribute('alt', 'Fermer le menu');

    }
}




eles = document.querySelectorAll("#lang > li > a");
eles[0].setAttribute('title', 'Basculer en français');
eles[1].setAttribute('title', 'Switch to English');

function check_IE(){
    if (/MSIE 10/i.test(navigator.userAgent)) {
       // This is internet explorer 10
       return true;
    }

    if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
        // This is internet explorer 9 or 11
        return true;
    }

    if (/Edge\/\d./i.test(navigator.userAgent)){
       // This is Microsoft Edge
       return true;
    }
    return false;
}

if(check_IE()){
    console.log('SOUS IE');
    var li =  document.querySelectorAll('li.menu-item > a');
    var item;
    for(var i = 0 ; i < li.length ; i++){

        item = li[i];
        item.addEventListener('focus', function(){
            if(this.parentElement.classList.contains('menu-item-has-children')){
                this.parentElement.classList.add('focus-within');
            }
        });

        item.addEventListener('blur', function(){
            if(this.parentElement.classList.contains('focus-within')){
                this.parentElement.classList.remove('focus-within');
            }
        });
    }
}
