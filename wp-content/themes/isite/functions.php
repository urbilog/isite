<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_parent_css' ) ):
    function chld_thm_cfg_parent_css() {
    }
endif;


function add_js_scripts() {
	wp_enqueue_script( 'script', get_stylesheet_directory_uri().'/js/appel_front.js', array('jquery'), '1.0', true );
	wp_localize_script('script', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
}


add_action('wp_enqueue_scripts', 'add_js_scripts');
add_action( 'wp_ajax_mon_action', 'mon_action' );
add_action( 'wp_ajax_nopriv_mon_action', 'mon_action' );

remove_action( 'wp_head', '_wp_render_title_tag', 1 ); //remove <title>


function mon_action() {
	global $wpdb;
	$table = $wpdb->prefix . "appels_a_projets" ;
	if($_POST['param'] != ''){
		// EN COURS
		$sql = "SELECT * FROM " . $table . " where cloture > NOW() AND id_appel_cat LIKE '%|".$_POST['param']."|%' order by lancement DESC";
		$appels = $wpdb->get_results($sql, OBJECT);
		// CLOS
		$sql = "SELECT * FROM " . $table . " where cloture < NOW() AND id_appel_cat LIKE '%|".$_POST['param']."|%' order by lancement DESC";
		$appels = array_merge($appels,$wpdb->get_results($sql, OBJECT));


	}else{
		$sql = "SELECT * FROM " . $table . " where cloture > NOW() order by lancement DESC";
		$appels = $wpdb->get_results($sql, OBJECT);

		$sql = "SELECT * FROM " . $table . " where cloture < NOW() order by lancement DESC";
		$appels = array_merge($appels,$wpdb->get_results($sql, OBJECT));
	}

	echo json_encode($appels);

	die();
}

// disable srcset
function meks_disable_srcset( $sources ) {
    return false;
}

add_filter( 'wp_calculate_image_srcset', 'meks_disable_srcset' );


if( !function_exists( 'get_slupy_search_form' ) ) {

	function get_slupy_search_form( $html ) {
	$html = '<form role="search" method="get" id="searchForm" action="'.home_url( '/' ).'">
					<label class="searchLabel" for="search">'._x( 'Search for:', 'label' ).'</label><input id="search" name="s"   type="search"/>
					<button id="submitSearch" type="submit" value="'.esc_attr_x( 'Search', 'submit button' ).'">
					<img alt="Lancer la recherche" src="'.WP_SITEURL.'/wp-content/uploads/2018/01/search.png"/>
					</button>
				</form>';
	  return $html;
	}
	add_filter( 'get_search_form', 'get_slupy_search_form' );
}

function get_term_lang(){
    $lang = get_bloginfo("language");
    $domaine = get_bloginfo("wpurl");
    $file = "fr.json" ;
    $rep  = "lang" ;
    switch ($lang) {
        case "fr-FR": $file = "fr.json" ;
            break;
        case "en-GB": $file = "en.json" ;
			break;
		  case "es": $file = "es.json";
      break;
    }
    $content = wp_remote_get($domaine.'/wp-content/themes/isite/lang/'.$file);
    return json_decode($content["body"]);
}

function my_mce_before_init_insert_formats( $init_array ) {
	// Define the style_formats array
	$style_formats = array(
		// Each array child is a format with it's own settings
		array(
			'title'    => 'Image en haut à droite',
			'classes'  => 'imgTopRight',
      'selector' => 'img',
			'wrapper' => false,
		),
    array(
			'title' => 'Image large',
			'classes' => 'imgLarge',
      'selector' => 'img',
			'wrapper' => false,
		),
    array(
			'title' => 'Picto gauche liste',
			'classes' => 'imgPictoLeftList',
      'selector' => 'img',
			'wrapper' => false,
		),
    array(
			'title' => 'Image actu droite',
			'classes' => 'imgActuRight',
      'selector' => 'img',
			'wrapper' => false,
		),
    array(
			'title' => 'Encadré orange paragraphe',
			'classes' => 'paragraph-orange',
      'selector' => 'p',
			'wrapper' => false,
		),
    array(
			'title' => 'Encadré bleu paragraphe',
			'classes' => 'paragraph-bleu',
      'selector' => 'P',
			'wrapper' => false,
		)
	);
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init_array['style_formats'] = json_encode( $style_formats );

	return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

// function myCustomPostType_per_page($query) {
// 	if (isset($query->query['post_type'])
// 	    && $query->query['post_type'] == 'app'
// 	) {
//     $query->set('orderby', 'fin_app');
//     $query->set( 'order', 'DESC' );
// 		$query->query_vars['posts_per_page'] = 5;
// 	}
// 	return $query;
// }
// add_filter('pre_get_posts', 'myCustomPostType_per_page');
//
add_action("admin_init", "portrait_manager_add_meta");
    function portrait_manager_add_meta() {
        add_meta_box(
            "post-meta",
            "Dans le câdre d'un portrait",
            "posts_manager_meta_options",
            "post",    // THIS PLACES THE BOX IN THE RIGHT SECTION
            "normal",       // PLACEMENT WITHIN SECTION
            "high"          // PRIORITY OF BOX
        );
    }

    function posts_manager_meta_options() {
        global $post;
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }
        $custom = get_post_custom($post->ID);
        $nom = $custom["nom"][0];
        $prenom = $custom["prenom"][0];
?>
        <div class="contact_manager_extras">
            <div>
                <label> Nom: </label>
                <input type="text" name="nom" value="<?php echo $nom; ?>"/>
            </div>
            <div>
                <label> Prénom: </label>
                <input type="text" name="prenom" value="<?php echo $prenom; ?>"/>
            </div>
        </div>
<?php
    }

    add_action("save_post", "post_manager_save_extras");
    function post_manager_save_extras() {
        global $post;
        if(defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }
        else {
            update_post_meta($post->ID, "nom", $_POST["nom"]);
            update_post_meta($post->ID, "prenom", $_POST["prenom"]);
        }
    }
