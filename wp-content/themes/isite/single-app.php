<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post(); ?>
		<div id="filter" class="appels_a_projets">
			<ul>
				<?php
				$args = array('hide_empty' => 0, 'hierarchical' => true, 'title_li' => '', 'taxonomy' => 'app_category', 'current_category' => 1, 'show_option_none'  => '',
				);
				$filtre = wp_list_categories($args); ?>
			</ul>
		</div>

		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<header class="entry-header">
				<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			</header>

			<div class="entry-content">
				<?php
					the_content();
				?>
			</div>
		</article>
		<?php $LANG = get_term_lang(); ?>
    <a class="seeAllAPP" href="<?php echo $LANG->ISITE_LIEN_APPELS_A_PROJET; ?>"><p id=""><?php echo $LANG->ISITE_APP_ALL; ?></p></a>
		<?php
		endwhile;
		?>
	</main>
</div>
<?php get_footer(); ?>
