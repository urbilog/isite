<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */
$GLOBALS['body'] = 'orange';
$LANG = get_term_lang();
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<article id="post-17" class="post-17 page type-page status-publisg hentry">
				<header class="entry-header">
					<h1 class="entry-title"><?php echo $LANG->ISITE_NOSRESULTATS; ?></h1>
				</header>
				<div class="entry-content">
					<div class="entry-content">
						<div id="main-top">
							<div id="top-wrap">
								<div id="main-panel">
									<h1><?php echo $LANG->ISITE_NOSRESULTATS; ?></h1>
								</div>
							</div>
						</div>
						<div id="filter" class="appels_a_projets"><ul>

						<?php
            $args = array('hide_empty' => 0, 'hierarchical' => true, 'title_li' => '', 'taxonomy' => 'app_category', 'current_category' => 1, 'show_option_none'  => '',
						);
							$filtre = wp_list_categories($args);

						?></div>
						<div id="les_appels" class="les_appels">
						<?php
							if ( have_posts() ) :
								$posts = get_posts([
									'post_type' => 'app',
									'post_status' => 'publish',
									'meta_key' => 'fin_app',
									'orderby' => 'meta_value',
									'order' => 'DESC',
									'numberposts' => -1
								]);
								foreach ( $posts as $post) {
								$date = new DateTime(get_field('fin_app'));
								$date_debut = new DateTime(get_field('date_de_debut_app'));
								$current_date = new DateTime();
								$post_lang = pll_get_post_language($post->ID , 'slug');

								switch($post_lang){
									case "en" : $project_open = 'project_open_en';
															$project_close = 'project_close_en';
										break;
									case "es" : $project_open = 'project_open_es';
															$project_close = 'project_close_es';
										break;
									case "fr" : $project_open = 'project_open';
															$project_close = 'project_close';
										break;
								}

								if($date_debut <= $current_date){
									if($current_date < $date){
										echo "<div class='project $project_open'>";
									}else{
										echo "<div class='project $project_close'>";
									}

								}
								echo '<h3>';
								echo get_the_title();
								echo '</h3>';
                echo do_shortcode(get_post_field('post_content', $post->ID));
							?>
							</div><?php } else : endif; ?>
						</div>
					</div>
				</div>
			</article>

		</main><!-- #main -->
</div><!-- .wrap -->

<?php get_footer();
