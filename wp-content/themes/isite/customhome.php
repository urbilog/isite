<?php
/*
Template Name: homepage
*/

get_header(); ?>

<div id="main" role="main">
    <div id="home">
		<h1 class="sr-only">Page d'accueil</h1>
		<?php echo do_shortcode("[Urbi-carrousel]"); ?>
		<?php echo do_shortcode("[Urbi-actus]"); ?>
	</div> 
<script> var posts = <?php echo $posts; ?>; </script>
<?php get_footer(); ?>
