<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$GLOBALS['color'] = 'orange';
get_header();
 ?>

<div id="content" role="main" class="link">
    <div id="profile">
        <div id="lastprofiles" class="filterContainer">
            <div id="filter">
                <ul>
				<?php
                    $args = array( 'hide_empty' => 1, 'title_li' => '');
                    wp_list_categories($args); ?>
                </ul>
            </div>
            <div class="profileDetail">
                <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();
                        get_template_part( 'template-parts/content', 'single' );
                    endwhile;
                ?>
                <div class="navigation_publi">
                <?php
                    previous_post_link('%link', '<p id="previous">'.$LANG->ISITE_PUBLICATION_PRECEDENTE.'</p>', true, '', 'category'); ?>
                    <a href="#"><p id=""><?php echo $LANG->ISITE_PUBLICATION_ALL; ?></p></a>
                <?php
                    next_post_link('%link', '<p id="next">'.$LANG->ISITE_PUBLICATION_SUIVANTE.'</p>',true, '', 'category');
                ?>
                </div>
                <div class="share">
                  <h2><?php echo $LANG->ISITE_PARTAGER; ?></h2>
                  <?php
                        $twitter = 'http://twitter.com/intent/tweet?text=' . get_the_title() . ':&amp;url='. get_the_permalink();
                        $linkedin = 'http://www.linkedin.com/shareArticle?mini=true&url='.get_the_permalink().'&title='.get_the_title().'&summary=&source='.get_bloginfo('name');
                    ?>
                    <ul>
                        <li>
                            <a href="<?php echo $twitter; ?>" target='_blank'>
                                <img alt="<?php echo $LANG->ISITE_PARTAGER_LINKEDIN_ALT; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-twitter-black.png">
                            </a>
                        </li>
                        <li>
                        <a href="<?php echo $linkedin; ?>" target='_blank'>
                                <img alt="<?php echo $LANG->ISITE_PARTAGER_TWITTER_ALT; ?>" src="<?= WP_SITEURL ?>/wp-content/images/linkedin-noir.png">
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
		// var html = document.documentElement;
		// if(html.lang == 'en-GB'){
		// 	var prev = document.getElementById("previous");
    //         if(prev != undefined){
    //             prev.innerText = "< Previous publication" ;
    //         }
    //         var next = document.getElementById("next");
    //         if(next != undefined){
    //             next.textContent = "Next publication >" ;
    //         }
		// }

	</script>
<?php get_footer("footer"); ?>
