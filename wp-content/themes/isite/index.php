<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
    <div id="main" role="main">
        <div id="news">
            <?php //echo do_shortcode("[Urbi-chiffres]"); ?>
            <div id="lastNews" class="filterContainer">
                <?php if(get_bloginfo("language") == 'en-GB'){ ?>
                    <h1>News</h1>
                <?php }else if(get_bloginfo("language") == 'es'){ ?>
                    <h1>Noticias</h1>
                <?php }else{ ?>
                    <h1>Actualités</h1>
                <?php } ?>
                    <div id="filter">
                        <ul>
                            <?php
                            $args = array('hide_empty' => 1, 'hierarchical' => true, 'title_li' => '');
                            wp_list_categories($args); ?>
                            <?php ?>
                        </ul>
                    </div>

                    <ul class="newsList list">
                    <?php
                    if ( have_posts() ) : ?>
                        <?php
                        while ( have_posts() ) : the_post();
                            get_template_part( 'template-parts/content', get_post_format());
                        endwhile;
                    ?>
                    <?php
                    else :
                        get_template_part( 'template-parts/content', 'none' );
                    endif;
                    ?>

                </ul>
                <div class="navigation_publi">
                    <?php if(get_bloginfo("language") == 'en-GB'){ ?>
                        <?php posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Previous news</p>','<p id="next">Next news <span aria-hidden="true">&gt;</span></p>'); ?>
                    <?php }else if(get_bloginfo("language") == 'es-ES'){ ?>
                        <?php posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Noticias anteriores</p>','<p id="next">Próxima noticia <span aria-hidden="true">&gt;</span></p>'); ?>
                    <?php }else{ ?>
                        <?php posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Actualités précédentes</p>','<p id="next">Actualités suivantes <span aria-hidden="true">&gt;</span></p>'); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
<?php get_footer(); ?>
