<?php
global $post;

define('IMG', '/wp-content/images/');

$GLOBALS['LANG'] = get_term_lang();
$GLOBALS['COULEUR'] = 'orange';

if($color = get_field('couleur', $post->ID)){
  $GLOBALS['COULEUR'] = $color;
}


if(get_category( get_query_var( 'cat' ))->slug == 'portraits-fr'){
  $GLOBALS['COULEUR'] = 'orange-light';
}
