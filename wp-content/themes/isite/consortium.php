<?php
/* Template Name: consortium */

get_header();
?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<?php if (get_locale() == 'en_GB') { ?>
		<ul class="partenaires">
			<li>
				<a href="http://www.univ-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/univ-lille.png" alt="Universite de Lille website" />
					<div class="overlay">
						<h2>Université de Lille</h2>
					</div>
				</a>

			</li>
			<li>
				<a href="http://www.ec-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/ec-lille.png" alt="Centrale Lille website" />
					<div class="overlay">
						<h2>Centrale Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.ensait.fr/">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/ensait.png" alt="Ecole Nationale Supérieure des Arts et Industries Textiles website" />
					<div class="overlay">
						<h2>Ecole Nationale Supérieure des Arts et Industries Textiles</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.ensc-lille.fr/">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/ensc-lille.png" alt="Ecole Nationale Supérieur de Chimie de Lille website" />
					<div class="overlay">
						<h2>Ecole Nationale Supérieure de Chimie de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.lille.archi.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/images/fondateurs/lille.archi.jpg" alt="École Nationale Supérieure d'Architecture et de Paysage de Lille website" />
					<div class="overlay">
						<h2>École Nationale Supérieure d'Architecture et de Paysage de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.esj-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/esj-lille.png" alt="Ecole Supérieure de Journalisme de Lille website" />
					<div class="overlay">
						<h2>Ecole Supérieure de Journalisme de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.sciencespo-lille.eu">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/sciencespo-lille.png" alt="Sciences Po Lille website" />
					<div class="overlay">
						<h2>Sciences Po Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.imt-lille-douai.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/imt-lille-douai.png" alt="Institut Mines-Télécom Lille-Douai Lille-Douai website" />
					<div class="overlay">
						<h2>Institut Mines-Télécom Lille-Douai</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.artsetmetiers.fr"> <img style="width:50%;" src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/artsetmetiers.jpg" alt=" Arts et Métiers Campus de Lille website" />
					<div class="overlay">
						<h2>Arts et Métiers Campus de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.chru-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/chru-lille.png" style="width: 25%;" alt="Centre Hospitalier Universitaire de Lille website" />
					<div class="overlay">
						<h2>Centre Hospitalier Universitaire de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.pasteur-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/pasteur-lille.png" alt="Institut Pasteur de Lille website" />
					<div class="overlay">
						<h2>Institut Pasteur de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.cnrs.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/wp-content/images/fondateurs/cnrs.png" alt="Centre national de la recherche scientifique website" />
					<div class="overlay">
						<h2>Centre national de la recherche scientifique</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.inria.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/images/inria.png" style="width:50%;" alt="Institut national de recherche en informatique et en automatique website" />
					<div class="overlay">
						<h2>Institut national de recherche en informatique et en automatique</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.inserm.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/inserm.png" alt="Institut national de la santé et de la recherche médicale website" />
					<div class="overlay">
						<h2>Institut national de la santé et de la recherche médicale</h2>
					</div>
				</a>
			</li>
		</ul>
	<?php }else{ ?>
		<ul class="partenaires">
			<li>
				<a href="http://www.univ-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/univ-lille.png" alt="Vers le site Universite de Lille" />
					<div class="overlay">
						<h2>Université de Lille</h2>
					</div>
				</a>

			</li>
			<li>
				<a href="http://www.ec-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/ec-lille.png" alt="Vers le site Centrale Lille" />
					<div class="overlay">
						<h2>Centrale Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.ensait.fr/">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/ensait.png" alt="Vers le site Ecole Nationale Supérieure des Arts et Industries Textiles" />
					<div class="overlay">
						<h2>Ecole Nationale Supérieure des Arts et Industries Textiles</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.ensc-lille.fr/">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/ensc-lille.png" alt="Vers le site Ecole Nationale Supérieur de Chimie de Lille" />
					<div class="overlay">
						<h2>Ecole Nationale Supérieure de Chimie de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.lille.archi.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/lille.archi.jpg" alt="Vers le site École Nationale Supérieure d'Architecture et de Paysage de Lille" />
					<div class="overlay">
						<h2>École Nationale Supérieure d'Architecture et de Paysage de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.esj-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/esj-lille.png" alt="Vers le site Ecole Supérieure de Journalisme de Lille" />
					<div class="overlay">
						<h2>Ecole Supérieure de Journalisme de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.sciencespo-lille.eu">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/sciencespo-lille.png" alt="Vers le site Sciences Po Lille" />
					<div class="overlay">
						<h2>Sciences Po Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.imt-lille-douai.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/imt-lille-douai.png" alt="Vers le site Institut Mines-Télécom Lille-Douai Lille-Douai" />
					<div class="overlay">
						<h2>Institut Mines-Télécom Lille-Douai</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.artsetmetiers.fr"> <img style="width:50%;" src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/artsetmetiers.jpg" alt="Vers le site Arts et Métiers Campus de Lille" />
					<div class="overlay">
						<h2>Arts et Métiers Campus de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.chru-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/chru-lille.png" style="width: 25%;" alt="Vers le site Centre Hospitalier Universitaire de Lille" />
					<div class="overlay">
						<h2>Centre Hospitalier Universitaire de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.pasteur-lille.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/pasteur-lille.png" alt="Vers le site Institut Pasteur de Lille" />
					<div class="overlay">
						<h2>Institut Pasteur de Lille</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.cnrs.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/cnrs.png" alt="Vers le site Centre national de la recherche scientifique" />
					<div class="overlay">
						<h2>Centre national de la recherche scientifique</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.inria.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/images/fondateurs/inria.png" style="width:50%;" alt="Vers le site Institut national de recherche en informatique et en automatique" />
					<div class="overlay">
						<h2>Institut national de recherche en informatique et en automatique</h2>
					</div>
				</a>
			</li>
			<li>
				<a href="http://www.inserm.fr">
					<img src="<?= WP_SITEURL ?>/wp-content/uploads/2018/01/fondateurs/inserm.png" alt="Vers le site Institut national de la santé et de la recherche médicale" />
					<div class="overlay">
						<h2>Institut national de la santé et de la recherche médicale</h2>
					</div>
				</a>
			</li>
		</ul>
	<?php } ?>
	</main>
</div>
<?php get_footer('footer.php'); ?>
