<?php
/*
Template Name: appelaprojets
*/

get_header('white'); ?>

<div class="site-main" role="main" id="main">
		<h1><?php the_title() ; ?></h1>
		<?php the_content() ; ?>
		<?php
			if(isset($_REQUEST['cat']) && $_REQUEST['cat'] !=''){
				show_appels_a_projets($_REQUEST['cat']);
			}else{
				show_appels_a_projets(0);
			}

		?>
		<?php //show_appels_a_projets(); ?>
		
</div> <!-- #content -->
<script> var posts = <?php echo $posts; ?>; </script>
<?php get_footer(); ?>
