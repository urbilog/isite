<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<li>
	<article id="post-<?php the_ID(); ?>">	
		<a href="<?php the_permalink('',get_the_ID()); ?>">
			<div class="thumbnailContainer article">	
				<div class="color color<?php 
							echo rand(1, 6);
							$cats = get_the_category();
							foreach($cats as $cat ){
								echo " ".$cat->slug;
							}
							?>"></div>
				<img class="thumbnail article" src="<?php echo get_the_post_thumbnail_url(); ?>"/>
			</div>
			<h3><?php the_title();?></h2>
			<p><?php echo wp_trim_words(strip_shortcodes(get_the_content()), 34, '(...)');?></p>
		</a>
	</article>
</li><!-- #post-## -->