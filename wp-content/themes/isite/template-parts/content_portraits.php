<?php
/**
 * The template part for displaying content
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
//$id = the_ID();
?>
<li>
	<article id="post-<?php the_ID(); ?>">	
		<a href="<?php the_permalink('',get_the_ID()); ?>" class="portrait">
			<div class="thumbnailContainer article">		
				<img class="thumbnail article" src="<?php echo get_the_post_thumbnail_url(); ?>"/>
			</div>
			<div class="thumbnail hover"></div>
			<div class="titleProfile">
				<h3><?php echo get_post_custom_values('prenom', get_the_ID())[0]; ?> <?php echo get_post_custom_values('nom', get_the_ID())[0]; ?></h3>
				<p><?php the_title();?></p>
			</div>
		</a>
	</article>
</li><!-- #post-## -->