<?php 
    /* Template Name: content-isite */ 
?>
	<?php the_title( '<h1>', '</h1>' ); ?>
	<?php twentysixteen_excerpt(); ?>
	<?php twentysixteen_post_thumbnail(); ?>
	<?php the_content(); ?>
