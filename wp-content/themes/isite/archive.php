<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
$category = get_the_category();
$cat = get_category( get_query_var( 'cat' ) );
get_header(''); ?>

<div id="content" role="main">
<?php
if(get_field('portrait', $cat->ID) == 'oui'){ ?>
	<div id="profile">
		<div id="lastprofiles" class="filterContainer">
<?php
}else{ ?>
	<div id="news">
		<div id="lastNews" class="filterContainer">
<?php
}

?>
<div id="filter">
    <ul>
			<?php
        $args = array( 'hide_empty' => 1, 'hierarchical' => true, 'title_li' => '');
        wp_list_categories($args); ?>
    </ul>
</div>

					<?php if($cat->slug != 'portraits'){ ?>
						<ul class="newsList list">
					<?php }else{ ?>
						<ul class="profilesList list">
					<?php } ?>


					<?php if ( have_posts() ) : ?>
						<?php
						// Start the loop
						while ( have_posts() ) : the_post();

							if($cat->slug != 'portraits-fr' && $cat->slug != 'portraits-en'){ ?>
							<?php get_template_part( 'template-parts/content', get_post_format());
							}else{ ?>
							<?php
								get_template_part('template-parts/content_portraits', get_post_format());
							}

						endwhile;
					?>
					<?php

					else :
						get_template_part( 'template-parts/content', 'none' );

					endif;
					?>

				</ul>
				<div class="navigation_publi">
					<?php
					    if(get_bloginfo("language") == 'en-GB'){
							posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Previous news</p>','<p id="next">Next news <span aria-hidden="true">&gt;</span></p>');
						}else if(get_bloginfo("language") == 'es-ES'){
							posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Noticias anteriores</p>','<p id="next">Próxima noticia <span aria-hidden="true">&gt;</span></p>');
						}else{
							posts_nav_link(' ','<p id="previous"><span aria-hidden="true">&lt;</span> Actualités précédentes</p>','<p id="next">Actualités suivantes <span aria-hidden="true">&gt;</span></p>');
						}
					?>
				</div>
			</div>
		</div>
	</div>
<?php get_footer(); ?>
