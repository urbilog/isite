<header id="masthead" class="site-header" role="banner">
	<div class="site-header-main">
		<ul id="quick-link">
			<li><a href="#main"><?php echo $LANG->ISITE_ALLER_AU_CONTENU; ?></a></li>
			<li><a href="#footer"><?php echo $LANG->ISITE_ALLER_AU_FOOTER; ?></a></li>
		</ul>
		<div id="home-link">
			<?php $custom_header_sizes = apply_filters( 'twentysixteen_custom_header_sizes', '(max-width: 709px) 85vw, (max-width: 909px) 81vw, (max-width: 1362px) 88vw, 1200px' );?>
			<div class="header-image">
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<?php if(!is_front_page()){ ?>
					<img alt="<?php echo $LANG->ISITE_PAGE_ACCUEIL; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-isite-noir-et-blanc.png"/>
				<?php }else{ ?>
					<img alt="<?php echo $LANG->ISITE_PAGE_ACCUEIL; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-isite.png"/>
				<?php } ?>
				</a>
			</div>
		</div>
		<?php if(is_front_page()){ ?>
			<a href="http://www.gouvernement.fr/secretariat-general-pour-l-investissement-sgpi">
				<img class="SIG" alt="<?php echo $LANG->ISITE_ALT_INVESTISSEMENT_AVENIR; ?>" src="<?= WP_SITEURL ?>/wp-content/images/LOGO_Investirlavenir_RVB.png"/>
			</a>
			<?php }?>

			<div class="menu-info">
				<div>
					<a href="<?php echo $LANG->ISITE_LIEN_NOUS_SOUTENIR; ?>">
						<img alt="" src="<?= WP_SITEURL ?>/wp-content/images/logo-nous-soutenir.png" />
						<p><?php echo $LANG->ISITE_NOUS_SOUTENIR; ?></p>
					</a>
					<a href="<?php echo $LANG->ISITE_LIEN_APPELS_A_PROJET; ?>">
						<img alt="" src="<?= WP_SITEURL ?>/wp-content/images/logo-appels-a-projet.png" />
						<p><?php echo $LANG->ISITE_APPELS_A_PROJET; ?></p>
					</a>
				</div>
				<div>
					<a href="https://twitter.com/isiteULNE" >
						<img alt="<?php echo $LANG->ISITE_ALT_TWITTER; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-twitter.png" />
					</a>
					<?php if(get_bloginfo("language") == 'es'){ ?>
						<a href="<?php echo get_home_url();?>/index.php/en/contact-3/">
							<img alt="<?php echo $LANG->ISITE_ALT_MESSAGE; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-message.png" />
						</a>
					<?php }else{ ?>
						<a href="<?php echo get_home_url();?>/index.php/en/contact-2/">
							<img alt="<?php echo $LANG->ISITE_ALT_MESSAGE; ?>" src="<?= WP_SITEURL ?>/wp-content/images/logo-message.png" />
						</a>
					<?php } ?>

				</div>
			</div>

		<?php ?>
	<div class="menu-principal">
		<a role="button" href="#" aria-hidden="true" onclick="burgerMenu(event);" id="menu-button"><img alt="Ouvrir le menu" src="<?= WP_SITEURL ?>/wp-content/images/logo-menu-hamburger-open.png"/></a>
		<nav>
			<?php wp_nav_menu(array('container'=>'', 'menu_id' => 'menu-nav')); ?>
			<?php wp_nav_menu(array('container'=>'', 'menu_id' => 'menu-nav-mobile')); ?>
			<?php  ?>
			<div class="searchLang">
			<?php get_search_form() ;?>
				<ul id="lang"><?php pll_the_languages();?></ul>
			</div>
		</nav>
	</div>
</header>
