<?php

function the_breadcrumb( $list = false, $separator = ' > ' ){
	global $post;
	global $wp;
    if( $list && !preg_match( '#<li#i', $separator ) ){
        $separator = '<li class="separator">' . $separator . '</li>';
    }
     
    if( $list ) echo '<ul class="ariane">';
    else echo '<div class="ariane">';



    if (!is_home()) {
        if( $list ) echo '<li>';
     
        echo '<a href="' . get_option( 'home' ) . '">' . __( 'Accueil' ) . '</a>';
 
        if( $list ){
            echo '</li>';
        }
 
        //echo $separator;
 
        if (is_category() || is_single()) {
 
			
			foreach( get_the_category() as $category){
				if(($wp->query_vars['category_name'] !== "") && ($wp->query_vars['category_name'] === $category->category_nicename)){
					echo $separator . $category->cat_name;
				}
			}
			
            /*if( $list ){
                echo '<li>';
                the_category(' </li>' . $separator . '<li> ');
            } else {
                the_category($separator);
            }*/
 
 
            if (is_single()) {
                if( $list ) echo '</li>' . $separator . '<li>';
                else echo $separator;
 
                the_title();
 
                if( $list ) echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '';
                    if( $list ) $output .= '<li>';
                    $output .= '<a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a>';
                    if( $list ) $output .= '</li>';
                    $output .= $separator;
                }
                echo $output;

                echo '<strong title="'.$title.'"> '.$title.'</strong>';
                 
                unset( $title );
            } else {
                if( $list ) echo '<li>';
                $menuParent =  get_menu_parent_label_by_post_id($post->ID, 'nav');
				if($menuParent !== get_the_title()){
					echo $separator . $menuParent;
				}
                echo $separator.get_the_title().'';
                if( $list ) echo '</li>';
            }
        }
     
    } elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) { $title = "Archive for "; the_time('F jS, Y'); }
    elseif (is_month()) { $title = "Archive for "; the_time('F, Y'); }
    elseif (is_year()) { $title = "Archive for "; the_time('Y'); }
    elseif (is_author()) { $title = "Author Archive"; }
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { $title =  "Blog Archives"; }
    elseif (is_search()) { $title = "Search Results"; }
     
    if( isset( $title ) ){
        if( $list ) $title = "<li>$title</li>";
        echo $title;
    }
     
    if( $list ) echo '</ul>';
    else echo '</div>';
}
/*if ( have_posts() ) : 
    while ( have_posts() ) : the_post();*/
          the_breadcrumb();
/*   endwhile; 
endif; */

?>