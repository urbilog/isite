<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title>Isite | <?php the_title(); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<?php wp_enqueue_script('isite', get_stylesheet_directory_uri().'/js/isite.js', null, null, true); ?>
	<?php wp_enqueue_script('objectfit', get_stylesheet_directory_uri().'/js/objectfit.js', null, null, true); ?>
	<?php
		$LANG = get_term_lang();
	?>
	<!-- <script src="<?= WP_SITEURL ?>/wp-content/themes/js/cookiechoices.js"></script> -->
</head>
