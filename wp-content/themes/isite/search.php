<?php
/**
 * The template for displaying search results pages
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if ( have_posts() ) : ?>

			<header class="page-header">
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
			</header>
			<div class="search-result">
			<?php
			while ( have_posts() ) : the_post();
				get_template_part( 'template-parts/content', 'search' );
			endwhile;
			?>
			</div>
			<?php
		else :
			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>
		</main>
	</section>
	<?php
	the_posts_pagination( array(
				'prev_text'          => __( 'Previous page', 'twentysixteen' ),
				'next_text'          => __( 'Next page', 'twentysixteen' ),
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>',
			) );
	?>
	<script>
		var html = document.documentElement; 
		console.log(html.lang);
		if(html.lang == 'en-GB'){
			var eles = document.querySelectorAll("#breadcrumb > span > span");
			var search = eles[0].innerText.split("'")[1];
			eles[0].innerText = "Search results for '" + search + "'";
		}else if(html.lang == 'es-ES'){
			var eles = document.querySelectorAll("#breadcrumb > span > span");
			var search = eles[0].innerText.split("'")[1];
			eles[0].innerText = "Resultados de búsqueda para '" + search + "'";
		}
		

	</script>
<?php get_footer(); ?>
