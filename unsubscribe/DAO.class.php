<?php

class DAO {
  public static function unsub($email, $token){
    $query = SPDO::getInstance()->prepare(
      "SELECT * FROM " . PREFIXE . "abonnes WHERE email = ? AND token = ?");
      
    return $query->execute(array(
      $email,
      $token
    ));
  }
}
