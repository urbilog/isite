<?php
include "conf.inc.php";
if(isset($_GET['lang'])){
  switch($_GET['lang']){
    case 'en-GB' :
      $translate = array(
        "TITRE" => "Unsubscribing",
        "BOUTON" => "I unsubscribe",
        "ERREUR" => "<ul><li>Check your email address</li><li>You may already be unsubscribed</li></ul>",
        "SUCCES" => "You are unsubscribed!",
        "REQUIS" => "The email field is required."
      );
    break;

    case 'fr-FR' :
      $translate = array(
        "TITRE"  => "Désabonnement",
        "BOUTON" => "Je me désabonne",
        "ERREUR" => "<ul><li>Vérifiez votre adresse email</li><li>Vous êtes peut être déjà désabonné</li></ul>",
        "SUCCES" => "Vous êtes désabonné !",
        "REQUIS" => "Le champ email est obligatoire"
      );
    break;

    case 'es-ES' :
      $translate = array(
        "TITRE" => "Unsubscribing",
        "BOUTON" => "I unsubscribe",
        "ERREUR" => "<ul><li>Check your email address</li><li>You may already be unsubscribed</li></ul>",
        "SUCCES" => "You are unsubscribed!",
        "REQUIS" => "The email field is required."
      );
    break;

    default:
      $translate = array(
        "TITRE" => "Désabonnement",
        "BOUTON" => "Je me désabonne",
        "ERREUR" => "<ul><li>Vérifiez votre adresse email</li><li>Vous êtes peut être déjà désabonné</li></ul>",
        "SUCCES" => "Vous êtes désabonné !",
        "REQUIS" => "Le champ email est obligatoire"
      );
    break;
  }
}
?>
<!DOCTYPE html>
<html lang="<?php echo $_GET['lang']; ?>">
	<head>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>ISITE - <?php //echo ; ?></title>
		<!-- <link rel="shortcut icon" href="<?php //echo PATH_IMG; ?>favicon.ico" type="image/x-icon"> -->
		<link type="text/css" href="<?php echo PATH_CSS; ?>" rel="stylesheet" media="all" />
		<!--[if gte IE 8]><link type="text/css" href="<?php echo PATH_CSS; ?>ie<?php echo PATH_CSS; ?>style.css" rel="stylesheet" media="all" /><![endif]-->
	</head>
  <body>
		<header>
      <h1><?php echo $translate['TITRE']; ?></h1>
		</header>
    <main>
      <?php
        if(isset($_POST['email']) && isset($_POST['token']) ){
          if(!empty($_POST['email'])){
            $query = SPDO::getInstance()->prepare(
              "SELECT * FROM " . PREFIXE . "abonnes WHERE email = ? AND token = ?");
            $query->execute(array($_POST['email'],$_POST['token']));
            $result = $query->fetchObject();

            if(!$result){
              echo '<span class="message">'.$translate['ERREUR'].'</span>' ;
            }else{
              $query = SPDO::getInstance()->prepare(
                "DELETE FROM " . PREFIXE . "abonnes WHERE email = ? AND token = ?");
              $delete = $query->execute(array($_POST['email'],$_POST['token']));

              if($delete){
                $query = SPDO::getInstance()->prepare(
                  "SELECT nombre FROM " . PREFIXE . "desabonnements");
                $query->execute();
                $result = $query->fetchObject();
                $result = intval($result->nombre) + 1;

                $query = SPDO::getInstance()->prepare(
                  "UPDATE " . PREFIXE . "desabonnements SET nombre = ?");
                $query->execute(array($result));

                $_GET['email'] = '';
                $_GET['token'] = '';

                echo '<span class="message">'.$translate['SUCCES'].'</span>' ;
              }
            }
          }else{
            echo '<span class="message">'.$translate['REQUIS'].'</span>' ;
          }
        }
      ?>
      <form method="post" action="">
        <label for="email">Email :</label>
        <input type="email" required name="email" id="email" value="<?php if(isset($_GET['email']) && !empty($_GET['email'])){ echo $_GET['email']; } ?>"/>
        <input type="hidden" name="token" id="token" value="<?php if(isset($_GET['token']) && !empty($_GET['token'])){ echo $_GET['token']; } ?>"/>
        <input type="submit" value="<?php echo $translate['BOUTON']; ?>" />
      </form>
    </main>
  </body>
</html>
