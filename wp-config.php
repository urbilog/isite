<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'isite');

/** MySQL database username */
define('DB_USER', 'isite');

/** MySQL database password */
define('DB_PASSWORD', 'isite');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

define('WP_SITEURL', 'http://localhost:8888/isite');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'sD7no+S7t2Tn0ZHBIk2n1Jx+giK8sJbSZja7gZ8pjer1T3tJX+lI/PxrAZa0');
define('SECURE_AUTH_KEY',  '8pSzosH3Po4To3tig/A7GH8Y55qQlFIVPQhCXGwg2+he6J8+/gz8Gnl1+CEy');
define('LOGGED_IN_KEY',    'iP5HeMi1a0f6bodgRt79gvjfFYX5zOtFJ8PZoMhSxYfkqLteKfY1Csk/MGsE');
define('NONCE_KEY',        'uL3glFnvgQD/TlxAkpiN2Xd4C91ubS0SXdEPfF5m9P56HkIqXAVIy9J3mdbQ');
define('AUTH_SALT',        'KCOiQy27KrXeXYe2fvaU1nxUHBz9xUHmHxUkLPLWh9i2C9sSuqaRAsDvll5G');
define('SECURE_AUTH_SALT', 'Afp/TjsBEVK6Q5ox1Mc+7nGAbZ3ez5MDSKAd75HKTDF6Vslm6XlaTzBJPqvT');
define('LOGGED_IN_SALT',   'g9aAR9gArf3+J8Z8iaCXhqhDDHXRUwDLuZPKHzkzyK7vEvyD38TxyVuMC7I2');
define('NONCE_SALT',       '9hYIHMnDipz/eLGh5yDC0V3w+K4W1gXEnfTAuXhHu8r6EUg8+1V1zpBm1eeZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wor2853_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/* Fixes "Add media button not working", see http://www.carnfieldwebdesign.co.uk/blog/wordpress-fix-add-media-button-not-working/ */
define('CONCATENATE_SCRIPTS', false );

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
